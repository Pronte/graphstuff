#!/bin/bash -e

if [[ "$1" == ""  ]]; then
        echo "usage: $(basename $0) nde-basename..." >&2
        exit 1
fi

if [ -z "$CLASSPATH" ]; then
        echo "You need to set the CLASSPATH before running this script!"
        exit 1
fi

base="$1"

grep -v "#" $base.nde | tail -n +2 | sort -n | uniq  > $base-s

java it.unimi.dsi.webgraph.BVGraph -g ArcListASCIIGraph $base-s $base
java it.unimi.dsi.webgraph.Transform symmetrize $base $base-symmetric
java it.unimi.dsi.webgraph.Transform arcfilter $base-symmetric $base-symmetric-noloops NO_LOOPS

#java it.unimi.dsi.webgraph.BVGraph -o -O -L $base





