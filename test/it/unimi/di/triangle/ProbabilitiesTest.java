package it.unimi.di.triangle;

import it.unimi.di.counter.KBottomCounterArray;
import it.unimi.di.counter.KBottomCounterArray.KBottomCounter;
import it.unimi.dsi.stat.SummaryStats;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.commons.math3.util.MathUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProbabilitiesTest {

	
private final static Logger LOGGER = LoggerFactory.getLogger( BottomKCounterTest.class );
	
	private static final int EXPERIMENTS = 100000;
	
	private static final int N = 1000;
	private static final int A = 40;
	private static final int B = 70;
	private static final int I = 10;
	private static final int U = A+B-I;
	private static final int K = 10;
	
	public static KBottomCounterArray setup(){
		KBottomCounterArray sketches = new KBottomCounterArray( K, 2, N );
		int nodes = 0;
		for( int i=0; i<B-I; i++ ){
			sketches.add( 0, nodes );
			nodes ++;
		}
		for( int i=0; i<A-I; i++ ){
			sketches.add( 1, nodes );
			nodes ++;
		}
		for( int i=0; i<I; i++ ){
			sketches.add( 0, nodes );
			sketches.add( 1, nodes );
			nodes++;
		}
		sketches.sort();
		return sketches;
	}
	
	
	
	public int runMaxACompareMaxB(){
		KBottomCounterArray sketches = setup();
		if( LOGGER.isTraceEnabled() ) LOGGER.trace( sketches.toString() );
		int maxA = sketches.get( 0 ).getMaximumRank();
		int maxB = sketches.get( 1 ).getMaximumRank();
		
		return Integer.compare( maxA, maxB );
		
	}
	
	public boolean maxAIsInBSketch(){
		KBottomCounterArray sketches = setup();
		if( LOGGER.isTraceEnabled() ) LOGGER.trace( sketches.toString() );
		int maxElA = sketches.get( 0 ).getElementMaximumRank();
		KBottomCounter ttt = sketches.intersectionCounter( 0, 1 );
		for ( int i=0; i<ttt.size(); i++ ) if( ttt.getSketch()[i]==maxElA ) return true;
		return false;
	}
	
	public boolean maxAIsInB(){
		KBottomCounterArray sketches = setup();
		if( LOGGER.isTraceEnabled() ) LOGGER.trace( sketches.toString() );
		int maxElA = sketches.get( 0 ).getElementMaximumRank();
		if( maxElA > B-I ) return true;
		return false;
	}
	
	@Test
	public void testMaxAIsInBSketch(){
		SummaryStats ssA = new SummaryStats();
		for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
			boolean answ = maxAIsInBSketch();
			if( answ ) ssA.add( 1 );
			System.out.println( "Fraction t_k(A) is in skecth B " + ssA.sum()/exp );
		}
		double exact = I / A;
		System.out.print( exact );
	}
	
	@Test
	public void testMaxAIsInB(){
		SummaryStats ssA = new SummaryStats();
		for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
			boolean answ = maxAIsInB();
			if( answ ) ssA.add( 1 );
			System.out.println( "Fraction t_k(A) is in B " + ssA.sum()/exp );
		}
		double exact = A / (double)(U-B);
		System.out.print( exact );
	}
	
	
	@Test
	public void testMaxACompareMaxB(){
		SummaryStats ssA = new SummaryStats();
		SummaryStats ssEqual = new SummaryStats();
		SummaryStats ssB = new SummaryStats();
		
		double union = (double)(A+B-I);
		double exactEqual = K/union;
		for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
			int ttt = runMaxACompareMaxB();
			if( ttt== -1 ) ssA.add( 1 );
			if( ttt== 0 ) ssEqual.add( 1 );
			if( ttt== 1 ) ssB.add( 1 );
			System.out.println( "Fraction t_k(A)<t_k(B) " + ssA.sum()/exp );
			System.out.println( "Fraction t_k(A)=t_k(B) " + ssEqual.sum()/exp );
			System.out.println( "Fraction t_k(A)>t_k(B) " + ssB.sum()/exp );
		}
		int expmaxB=N/B*K;
		int expmaxA=N/A*K;
		System.out.println( "expmaxA " + expmaxA + " expmaxB " + expmaxB );
		
		Assert.assertEquals( "Fraction t_k(A)=t_k(B) " + ssEqual.sum()/EXPERIMENTS + " and exact " + exactEqual ,  exactEqual, ssEqual.sum()/EXPERIMENTS, exactEqual/10. );	
		//Assert.assertEquals( "Fraction t_k(A)<t_k(B) " + ssA.sum()/EXPERIMENTS + " and exact " + A/union ,  A/union, ssA.sum()/EXPERIMENTS, exact/100. );	
	}
	
	public static void main( String[] args ){
		new ProbabilitiesTest().testMaxAIsInB();
		//.testMaxACompareMaxB();
	}
}
