package it.unimi.di.counter.test;

import it.unimi.di.counter.BridgeHyperLogLogCounterArray;
import it.unimi.di.counter.CounterArray;
import it.unimi.di.counter.JaccardCounterArray;
import it.unimi.di.counter.JaccardKMins;
import it.unimi.di.counter.KBottomJaccardCounterArray;
import it.unimi.di.counter.KPartitionCounterArray;
import it.unimi.dsi.fastutil.booleans.BooleanArrays;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.util.XorShift1024StarRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;

public class CounterTest {
	
	static int n = 10000000;
	static int k = 12;
	static int item = 10000;
	static int experiments = 1000;
	static double step = 0.1;
	static int repeat = (int)(Math.log( n )+1);
	static String myClass = "";
	
	
	private final static Logger LOGGER = LoggerFactory.getLogger( CounterTest.class );
	private final static boolean DEBUG_ENABLED = true;
	private final static boolean PRINT_SIZE = true;
	private static final boolean TEST_UNION_INTERSECTION = false;
	private static final boolean TEST_JACCARD = true;
	
	private static int infiniteUnionError=0;
	private static int infiniteIntersectionError=0;
	private static boolean modifiederror=true;
	
	private static SummaryStats jaccardErrorStats;
	
	public static void main( String[] arg ) throws JSAPException{
		
		SimpleJSAP jsap = new SimpleJSAP( CounterTest.class.getName(), "Test Counters, by assigning elements of a universe to two sets A and B, by varying their probability to get elements",
				new Parameter[] {
					new UnflaggedOption( "class", JSAP.STRING_PARSER, JSAP.REQUIRED, "The class to be test" ),
					new FlaggedOption( "u", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'u', "universesize", "The size of the universe" ),
					new FlaggedOption( "k", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "The logarithm of the number of registers (log2m for HyperLogLog) or k (Cohen)" ),
					new FlaggedOption( "i", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'i', "item", "The items (The union size of A and B)" ),
					new FlaggedOption( "e", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed for each pair of probabilities" ),
					new FlaggedOption( "s", JSAP.DOUBLE_PARSER, "0.1", JSAP.NOT_REQUIRED, 's', "step", "The step for each probability" ),
					new FlaggedOption( "w", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'w', "while", "repeat each estimate w times and return the mean" ),
					new Switch( "r", 'r', "modifiederror", "If true compute a modified version of the relative error: estimate+1/truth+1" ),
					new Switch( "p", 'p', "point", "If true perform a test with given A-probability and B-probability: require to set a and b" ),
					new FlaggedOption( "a", JSAP.DOUBLE_PARSER, "0.1", JSAP.NOT_REQUIRED, 'a', "a", "A-probability" ),
					new FlaggedOption( "b", JSAP.DOUBLE_PARSER, "0.9", JSAP.NOT_REQUIRED, 'b', "b", "B-probability" ),
					
			});
		
		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );
		
		boolean point = jsapResult.getBoolean( "p" );
		
		if( point && !( jsapResult.userSpecified( "a" ) && jsapResult.userSpecified( "b" ) ) ){
			LOGGER.error( "For a single experiment you must set a and b" );
			System.exit( 1 );
		}
		if( point && jsapResult.userSpecified( "s" ) ){
			LOGGER.error( "For a single experiment you must not set s" );
			System.exit( 1 );
		}
		if( !point && ( jsapResult.userSpecified( "a" ) || jsapResult.userSpecified( "b" ) ) ){
			LOGGER.error( "For a multiple experiments you must not set a and b" );
			System.exit( 1 );
		}
		
		n = jsapResult.getInt( "u" );
		k = jsapResult.getInt( "k" );
		item = jsapResult.getInt( "i" );
		experiments = jsapResult.getInt( "e" );
		step = jsapResult.getDouble( "s" );
		modifiederror = jsapResult.getBoolean( "r" );
		repeat = jsapResult.getInt( "w" ); 
		myClass = jsapResult.getString( "class" ); 
		
		if( ! (myClass.equals( "KBottomRaw" ) || myClass.equals( "KBottomDensity" ) || myClass.equals( "KBottomThorup" ) || myClass.equals( "KMins" ) || myClass.equals( "KPartition" ) || myClass.equals( "HyperLogLog" ) ) ) {
			LOGGER.error( "The class " + myClass + " does not correspond to any option.");
			LOGGER.error( "Possible options are: KBottom, KBottomModified, KMins, KPartition, HyperLogLog" );
			System.exit( 1 );
		}
		
		if( point ){
			double thA =jsapResult.getDouble( "a" );
			double thAB =jsapResult.getDouble( "b" );
			if( thA > thAB ){
				LOGGER.error( "b has to be greater than a" );
				System.exit( 1 );
			}
			jaccardErrorStats = new SummaryStats();
			iterateExperiments( thA, thAB );
		} else {
			jaccardErrorStats = new SummaryStats();
			for( double thA=0.1; thA<1; thA=thA+step )
				for( double thAB=thA; thAB<1; thAB=thAB+step ){
					iterateExperiments( thA, thAB );
				}	
		}
		
		System.out.println( "infiniteUnionError: " + infiniteUnionError + " infiniteIntersectionError:" + infiniteIntersectionError );
		if(TEST_JACCARD) LOGGER.info( "Relative error (modified) Jaccard Coefficient Estimations: " + jaccardErrorStats);
		
	}


	private static void iterateExperiments( double thA, double thAB ) {
		double thB=1;
		String param = " A=" + thA + " AB=" +thAB + " B=" + thB + " ";
		LOGGER.info( "**************************************" );
		LOGGER.info( "PARAMETERS: " + param + " " + myClass );
		SummaryStats unionErrorStats = new SummaryStats();
		SummaryStats intersectionErrorStats = new SummaryStats();
		for( int ex = 0; ex<experiments; ex++ ){
			LOGGER.info( "RUNNING EXPERIMENT: " + ex );
			if( myClass.equals( "KBottomRaw" ) ) {
				String KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY = "RawKBottomCounterJaccardEstimateStrategy";
				new CounterTest().testKBottom( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat, KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY );
			}
			if( myClass.equals( "KBottomDensity" ) ) {
				String KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY = "DensityBasedKBottomCounterJaccardEstimateStrategy";
				new CounterTest().testKBottom( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat, KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY );
			}
			if( myClass.equals( "KBottomThorup" ) ) {
				String KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY = "ThorupKBottomCounterJaccardEstimateStrategy";
				new CounterTest().testKBottom( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat, KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY );
			}
			if( myClass.equals( "KMins" ) ) new CounterTest().testKMins( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat );
			//new CounterTest().testKSandwichCounterArray( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat );
			if( myClass.equals( "KPartition" ) ) new CounterTest().testKPartitionCounterArray( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat );
			if( myClass.equals( "HyperLogLog" ) ) new CounterTest().testHyperLogLog( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat );
		}
		if(!PRINT_SIZE) System.out.println( param + "UNION " + unionErrorStats.toString() );
		if(!PRINT_SIZE) System.out.println( param + "INTERSECTION " + intersectionErrorStats.toString() );
	}
	
	
	
	public void testHyperLogLog( int iterate, double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats, int repeat ) {
		BridgeHyperLogLogCounterArray[] counterArray = new BridgeHyperLogLogCounterArray[repeat];
		for(int j=0; j<counterArray.length; j++){
			BridgeHyperLogLogCounterArray ca = new BridgeHyperLogLogCounterArray( 2, n, k, new XorShift1024StarRandom().nextLong() );
			counterArray[j] = ca;
		}
		test( iterate, thA, thAB, thB, unionErrorStats, intersectionErrorStats, counterArray );
	}
	
	public void testKBottom( int iterate, double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats, int repeat, String KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY ) {
		KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
		for(int j=0; j<counterArray.length; j++){
			KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY );
			counterArray[j] = ca;
		}
		test( iterate, thA, thAB, thB, unionErrorStats, intersectionErrorStats, counterArray );
	}

	public void testKMins( int iterate, double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats, int repeat ) {
		JaccardKMins[] counterArray = new JaccardKMins[repeat];
		for(int j=0; j<counterArray.length; j++){
			JaccardKMins ca = new JaccardKMins( k, 2, n );
			counterArray[j] = ca;
		}
		test( iterate, thA, thAB, thB, unionErrorStats, intersectionErrorStats, counterArray );
	}
	
	public void testKPartitionCounterArray( int iterate, double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats, int repeat ) {
		KPartitionCounterArray[] counterArray = new KPartitionCounterArray[repeat];
		for(int j=0; j<counterArray.length; j++){
			KPartitionCounterArray ca = new KPartitionCounterArray( k, 2, n );
			counterArray[j] = ca;
		}
		test( iterate, thA, thAB, thB, unionErrorStats, intersectionErrorStats, counterArray );
	}

	private void test( int iterate, double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats, CounterArray[] counterArray ) {
		System.out.println( "testKBottom iterate:" + iterate );
		int aCount = 0;
		int bCount = 0;
		int intersection = 0;
		int a=0;
		int b=1;
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		
		boolean marked[] = new boolean[ n ];
		BooleanArrays.fill( marked, false );
		
		for( int i = 1; i<= iterate; i++ ){
			double coin = random.nextDouble();
			int element = random.nextInt( n );
			if( marked[element] ) continue;
			marked[element]=true;
			if ( coin < thA ){
				for(int j=0; j<counterArray.length; j++){
					counterArray[j].add( a, element );
				}
				aCount++;
			} else if ( coin < thAB ) {
				intersection++;
				for(int j=0; j<counterArray.length; j++){
					counterArray[j].add( a, element );
					counterArray[j].add( b, element );
				}
				aCount++;
				bCount++;
			} else if ( coin < thB ){
				for(int j=0; j<counterArray.length; j++){
					counterArray[j].add( b, element );
				}
				bCount++;
			}
			int union = aCount+bCount-intersection;

			if( i%iterate == 0){
				SummaryStats intersectionStats = new SummaryStats();
				SummaryStats jaccardStats = new SummaryStats();
				for(int j=0; j<counterArray.length; j++){

					double aEstimate = counterArray[j].count( a );
					//if (DEBUG_ENABLED) LOGGER.debug( "A-size: " + aCount + " A-estimate: " + aEstimate );
					if (DEBUG_ENABLED) LOGGER.debug( "A-size: " + aCount );
					double bEstimate = counterArray[j].count( b );
					//if (DEBUG_ENABLED) LOGGER.debug( "B-size: " + bCount + " B-estimate: " + bEstimate );
					if (DEBUG_ENABLED) LOGGER.debug( "B-size: " + bCount );
					
					if( TEST_JACCARD && counterArray[j] instanceof JaccardCounterArray ){
						double jj=((JaccardCounterArray)counterArray[j]).jaccardCoefficient( a, b );
						jaccardStats.add( jj );
						if (PRINT_SIZE) LOGGER.info( "JACCARD: " + intersection/(double)union + " JACCARD-estimate: " + jj );
					}

					if (TEST_UNION_INTERSECTION){
						double unionEstimate = counterArray[j].union( a, b );
						double intersectionEstimate = Math.max( 0, aEstimate + bEstimate - unionEstimate );
						intersectionStats.add( intersectionEstimate );
						double unionError = 0;
						double intersectionError = 0;
						if( union==0 ){
							unionError = (unionEstimate!=0 ) ? Double.MAX_VALUE: 0;
						}else unionError = unionEstimate/union;
						if( intersection==0 ){
							intersectionError = (intersectionEstimate!=0 ) ? Double.MAX_VALUE: 0;
						}else intersectionError = intersectionEstimate/intersection;
						if( unionError == Double.MAX_VALUE ) infiniteUnionError++;
						if( intersectionError == Double.MAX_VALUE ) infiniteIntersectionError++;
						if( modifiederror ) {
							unionError = (unionEstimate+1)/(union+1);
							intersectionError = (intersectionEstimate+1)/(intersection+1);
						}
						if (PRINT_SIZE) LOGGER.info( "UNION: " + union + " UNION-estimate: " + unionEstimate + " UNION-error: " + unionError );
						if (PRINT_SIZE) LOGGER.info( "INTERSECTION: " + intersection + " INTERSECTION-estimate: " + intersectionEstimate + " INTERSECTION-error: " + intersectionError  );
						unionErrorStats.add( (unionEstimate+1) / (union+1) );
						intersectionErrorStats.add( (intersectionEstimate+1) / (intersection+1) );
					}

				}
				if (TEST_UNION_INTERSECTION){
					double meanIntersection = intersectionStats.mean();
					double intersectionError = (meanIntersection+1)/(intersection+1);
					if (PRINT_SIZE) LOGGER.info( "ITERATIVE INTERSECTION: " + intersection + " INTERSECTION-estimate: " + meanIntersection + " INTERSECTION-error: " + intersectionError  );
				}	
				LOGGER.debug( "UNION " + union );
				LOGGER.debug( "INTERSECTION " + intersection );
				if (TEST_JACCARD){
					double jaccard = intersection/(double)union;
					double meanJaccard = jaccardStats.mean();
					double jaccardError = (meanJaccard+1)/(jaccard+1);
					jaccardErrorStats.add( jaccardError );
					if (PRINT_SIZE) LOGGER.info( "ITERATIVE JACCARD: " + jaccard + " JACCARD-estimate: " + meanJaccard + " JACCARD-error: " + jaccardError  );
				}
			}		
		}
	}
	 
}
