package it.unimi.di.counter;

import it.unimi.dsi.fastutil.booleans.BooleanArrays;
import it.unimi.dsi.util.XorShift1024StarRandom;

public class LogCounterArray {

	private LogCounter[] counterArray;
	 
	public LogCounterArray( int r, int m ){
		counterArray = new LogCounter[ m ];
		for( int i=0; i<counterArray.length; i++) counterArray[i] = new LogCounter( r );
	}
	
	
	public LogCounterArray( LogCounter[] counterArray ) {
		this.counterArray = counterArray;
	}

	public void init(){
		for( int i=0; i<counterArray.length; i++) counterArray[i].init();
	}
	
	public void add( LogCounter[] param ){
		LogCounter[] result = new LogCounter[ counterArray.length ]; 
		for( int i=0; i<counterArray.length; i++ )
			result[i] = counterArray[i].or( param[i] );
		counterArray = result;
	}
	
	public void add( LogCounterArray param ){
		add( param.getCounterArray() );
	}
	
	public LogCounter[] or( LogCounter[] param ){
		LogCounter[] result = new LogCounter[ counterArray.length ]; 
		for( int i=0; i<counterArray.length; i++ )
			result[i] = counterArray[i].or( param[i] );
		return result;
	}
	
	public LogCounter[] and( LogCounter[] param ){
		LogCounter[] result = new LogCounter[ counterArray.length ]; 
		for( int i=0; i<counterArray.length; i++ )
			result[i] = counterArray[i].and( param[i] );
		return result;
	}
	
	public LogCounterArray or( LogCounterArray param ){
		return new LogCounterArray( or( param.getCounterArray() ) );
	}
	
	public LogCounterArray and( LogCounterArray param ){
		return new LogCounterArray( and( param.getCounterArray() ) );
	}
	
	private LogCounter[] getCounterArray() {
		return counterArray;
	}
	
	public String toString(){
		String result = "[";
		for( int i = 0; i<counterArray.length ; i++)
			result+= counterArray[i].toString()+" ";
		result += "]";
		return result;
	}

	public double count(){
		int sum = 0;
		for( int i=0; i<counterArray.length; i++){
			sum += counterArray[i].getLeastZeroBit();
		}
		double avg = (double) sum / counterArray.length;
		return Math.pow( 2, avg ) / 0.77351;
	}
	
	public LogCounterArray copy(){
		LogCounter[] result = new LogCounter[ counterArray.length ];
		for( int i=0; i<counterArray.length; i++) result[i] = counterArray[i].copy();
		return new LogCounterArray( result );
	}
	
	static double[] mathPow2;
	
	public static void setMathPow2( int r ){
		mathPow2=new double[r];
		for( int i=0; i<mathPow2.length-1; i++ ) {
			mathPow2[i]=1./Math.pow( 2, i+1 );
			//System.out.println( "1 over 2 pow (" + i + "+1)" + mathPow2[ i ] );
		}
	}
	
	public static double getMathPow2( int i ){
		return mathPow2[i];
	}
	
	
	public class LogCounter{
		
		boolean[] counter;
		
		public LogCounter( int r ){
			counter = new boolean[ r ];
		}
		
		public LogCounter( boolean[] counter ){
			this.counter = counter;
		}
		
		public String toString(){
			String result = "";
			for( int i = 0; i<counter.length ; i++)
				if( counter[i] ) result+= "1";
				else result+= "0";
			result += " ";
			return result;
		}
		
		public void init(){
			XorShift1024StarRandom random = new XorShift1024StarRandom();
			double coin = random.nextDouble();
			for( int i=0; i<counter.length-1; i++ ){
				if( coin > LogCounterArray.getMathPow2( i ) ){
					counter[ i ] = true;
					return;
				}
			}
			//System.out.println( "WARING: last bit set coin=" + coin  );
			if( counter.length>0 ) counter[ counter.length-1 ]=true;
		}
		
		public boolean[] getCounter(){
			return counter;
		}
		
		public LogCounter copy(){
			boolean[] result = BooleanArrays.copy( counter );
			return new LogCounter( result );
		}
		
		public LogCounter or( LogCounter param ){
			boolean[] result = new boolean[ counter.length ];
			for( int i=0; i<counter.length; i++ ) result[ i ] = counter[ i ] || param.getCounter()[i];
			return new LogCounter( result );
		}
		
		public LogCounter and( LogCounter param ){
			boolean[] result = new boolean[ counter.length ];
			for( int i=0; i<counter.length; i++ ) result[ i ] = counter[ i ] && param.getCounter()[i];
			return new LogCounter( result );
		}
		
		public int getLeastZeroBit(){
			for(int i=0; i<counter.length; i++)
				if( counter[ i ] == false ) return i;
			return Integer.MIN_VALUE;
		}
		
		
	}
	
}
