package it.unimi.di.counter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DensityKBottomCounterArray extends KBottomCounterArray {

	private final static Logger LOGGER = LoggerFactory.getLogger( DensityKBottomCounterArray.class );
	
	public DensityKBottomCounterArray( int k, int m, int n ) {
		super( k, m, n );
	}
	
	public DensityKBottomCounterArray( int k, int m, int n, boolean allowCollision, int seed ) {
		super( k, m, n, allowCollision, seed ); 
	}
	
	public double jaccardCoefficient( long a, long b ) {
		return counters[(int)a].jaccardCoefficient( counters[(int)b] );
	}
	
	public double jaccardCoefficientClassicalWrapper( long a, long b ) {
		return super.jaccardCoefficient( a, b );
	}
	
	@Override
	public void prepare(){
		if( LOGGER.isTraceEnabled() ) LOGGER.info( "Calling Prepare..." );
		sort();
	}

	
	
}
