package it.unimi.di.triangle.stats;

import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.stat.SummaryStats;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;

public class GetDistributionOfTriangles {

	public static void main( String[] args ) throws JSAPException, IOException{
		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Compute ditribution",
				new Parameter[] {
						new FlaggedOption( "target", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 't', "target", "the target file" ),
				} );


		JSAPResult jsapResult = jsap.parse( args );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		double maxTarget = -1;
		int maxid = -1;
		List<String> values = Files.readLines( new File( jsapResult.getString( "target" ) ), Charsets.UTF_8 );
		double[] targetValues = new double[ values.size() ];
		for ( String line : values ) {
			int id = Integer.parseInt( line.split( " " )[ 0 ] );
			double value = Double.parseDouble( line.split( " " )[ 1 ] );
			targetValues[ id ] = value;
			if ( value > maxTarget ) {
				maxTarget = value;
				maxid = id;
			}
		}
		System.out.println( "Maximum target is " + maxTarget + "because of node " + maxid );
		long[] targetDistribution = new long[ (int)maxTarget +1 ]; 
		for ( int i =0; i< targetValues.length; i++ ) {
			targetDistribution[ (int)targetValues[i] ] ++;
		}
		
		for( int i=0; i<targetDistribution.length; i++ ) if( targetDistribution[i]!=0 ) System.out.println( i + " " +targetDistribution[i] );
		
	}
	
}
