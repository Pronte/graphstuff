package it.unimi.di.counter;

public abstract class JaccardCounterArray {

	public abstract double jaccardCoefficient( long a, long b );
	
	public abstract void add( long a, long element ) ;
	
	public void prepare(){}
}
