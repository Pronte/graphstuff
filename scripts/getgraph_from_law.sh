#!/bin/bash -e

if [[ "$1" == ""  ]]; then
        echo "usage: $(basename $0) graphBaseName..." >&2
        exit 1
fi

if [ -z "$CLASSPATH" ]; then
        echo "You need to set the CLASSPATH before running this script!"
        exit 1
fi

base="$1"
wget http://data.law.di.unimi.it/webdata/$base/$base.graph
wget http://data.law.di.unimi.it/webdata/$base/$base.properties

java it.unimi.dsi.webgraph.BVGraph -o -O -L $base





