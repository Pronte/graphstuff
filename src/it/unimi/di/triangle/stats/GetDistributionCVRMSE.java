package it.unimi.di.triangle.stats;
import java.io.File;
import java.io.IOException;
import java.util.List;

import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.stat.SummaryStats;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.UnflaggedOption;


public class GetDistributionCVRMSE {
	
	
	public static void main( String[] args ) throws JSAPException, IOException{
		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Given a list of files (two columns id value) separated by , compute id and means of values. ",
				new Parameter[] {
						new FlaggedOption( "target", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 't', "target", "the target file" ),
						new UnflaggedOption( "file", JSAP.STRING_PARSER, JSAP.REQUIRED, "The file to be processed." )
				} );
		JSAPResult jsapResult = jsap.parse( args );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		String targetName = jsapResult.getString( "target" );
		String fileName = jsapResult.getString( "file" );
		List<String> a1 = Files.readLines( new File( fileName ), Charsets.UTF_8 );
		List<String> a2 = Files.readLines( new File( targetName ), Charsets.UTF_8 );
		
		if( a1.size() != a2.size() ) throw new RuntimeException( "The two files must have the same number of lines" );
		
		double[] approx = new double[ a1.size() ];
		double[] exact = new double[ a2.size() ];
		
		for( String line : a1 ){
			int id= Integer.parseInt( line.split( " " )[0] );
			double value = Double.parseDouble( line.split( " " )[1] );
			approx[ id ] = value;
		}
		for( String line : a2 ){
			int id= Integer.parseInt( line.split( " " )[0] );
			double value = Double.parseDouble( line.split( " " )[1] );
			exact[ id ] = value;
		}
		
		SummaryStats ss = new SummaryStats();
		for( int i =0 ; i< approx.length; i++ ){
			if( exact[i]==0 ) continue;
			
			double vv = Math.sqrt((approx[i]-exact[i])*(approx[i]-exact[i]) ) / exact[i];
			if( vv > 2) System.out.println( "i=" + i + " approx[i]=" + approx[i] + " " + " exact[i]=" + exact[i] + " final "+ vv );
			ss.add( vv  );
		}
		System.out.println( ss.toString() );
		
	}

}
