package it.unipi.di.centralities;

import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.LazyIntIterator;
import it.unimi.dsi.webgraph.NodeIterator;

public class Slow {

	ImmutableGraph graph;
	int[][][] kBtw;
	int[][][] lessThanKBtw;
	int limit;
	int k = 1;
	
	public Slow( ImmutableGraph graph, int limit ){
		this.graph = graph;
		this.limit = limit;
		kBtw = getNewCube( graph.numNodes() );
		lessThanKBtw = getNewCube( graph.numNodes() );
		NodeIterator iter = graph.nodeIterator();
		k = 1;
		while( iter.hasNext() ){
			int curr = iter.nextInt();
			lessThanKBtw[ curr ][ curr ][ curr ] = 1;
			LazyIntIterator successors = graph.successors( curr );
			int d = graph.outdegree( curr );	
			while( d-- != 0 ) {
				int succ = successors.nextInt();
				kBtw[ succ ][ curr ][ succ ] = 1; //succ and curr are both on the shortest path of length 1 from curr to succ
				kBtw[ curr ][ curr ][ succ ] = 1;
				lessThanKBtw[ succ ][ curr ][ succ ] = 1;
				lessThanKBtw[ curr ][ curr ][ succ ] = 1;
			}
		}
	}
	
	public void nextIteration(){
		k++;
		int[][][] newM = getNewCube( graph.numNodes() );
		for( int u =0; u < graph.numNodes(); u++ ){
			for( int v =0; v < graph.numNodes(); v++ ){
				for( int w =0; w < graph.numNodes(); w++ ){
					if( lessThanKBtw[ u ][ v ][ w ] > 0 ) continue; //the shortest path from v to w is shorter than k
					/*
					LazyIntIterator successors = graph.successors( w );
					int d = graph.outdegree( w );
					while( d-- != 0 ) {
						int succ = successors.nextInt();
						newM[ u ][ v ][ w ] += kBtw[ u ][ v ][ succ ];
					}*/
					LazyIntIterator successors = graph.successors( v );
					int d = graph.outdegree( v );
					while( d-- != 0 ) {
						int succ = successors.nextInt();
						newM[ u ][ v ][ w ] += kBtw[ u ][ succ ][ w ];
					}
				}	
			}
		}
		kBtw = newM;
		for( int u =0; u < graph.numNodes(); u++ )
			for( int v =0; v < graph.numNodes(); v++ )
				for( int w =0; w < graph.numNodes(); w++ )
					lessThanKBtw[ u ][ v ][ w ] += kBtw[ u ][ v ][ w ];
	}
	
	public int[][][] getNewCube( int dimension ){
		int[][][] cube = new int[ dimension ][ dimension ][ dimension ];
		for( int i = 0; i< dimension; i++ ) {
			cube[ i ]=new int[ dimension ][ dimension ];
			for( int j = 0; j< dimension; j++ ) {
				cube[ i ][ j ] = new int[ dimension ];
			}
		}
		return cube;
	}
	
	public long getSum( int[][] array ){
		int sum = 0;
		for( int i = 0; i< array.length; i++ ) {
			for( int j = 0; j< array[i].length; j++ ) {
				sum+= array[ i ][ j ];
			}
		}
		return sum;
	}
	
	public void print( int[][][] array ){
		for( int i = 0; i< array.length; i++ ){
			System.out.println( "=========================" + i );
			print( array[i] );
		}
			
	}
	
	public void print( int[][] array ){
		for( int i = 0; i< array.length; i++ ) {
			for( int j = 0; j< array[i].length; j++ ) {
				System.out.print( array[i][j] + " " );
			}
			System.out.println();
		}
	}
	
	public void run(){
		System.out.println("Starting...");
		print( lessThanKBtw );
		for ( int i=0; i<limit; i++){
			nextIteration();
			System.out.println( "Iteration " + ( i+1 ) );
			//System.out.println( "kBtw" );
			//print( kBtw );
			System.out.println( "lessThanKBtw" );
			print( lessThanKBtw );
		}
		System.out.println("Result...");		
		for( int v=0; v<graph.numNodes(); v++ ){
			System.out.println( v + " " + getSum( lessThanKBtw[v] ) );
		}
	}
	
	
	
}
