package it.unimi.di.triangle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.di.counter.JaccardCounterArray;
import it.unimi.dsi.fastutil.doubles.DoubleArrays;
import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.LazyIntIterator;
import it.unimi.dsi.webgraph.NodeIterator;

public abstract class ApproximateTriangleCounter {

	private final static Logger LOGGER = LoggerFactory.getLogger( ApproximateTriangleCounter.class );

	ImmutableGraph graph;
	int k;
	static final int DEFAULT_SEED = 131;
	int seed;
	boolean collisions;
	JaccardCounterArray sketches;
	
	public ApproximateTriangleCounter( ImmutableGraph graph, int k, int seed, boolean collisions ){
		this.graph = graph;
		this.k = k;
		this.seed = seed;
		this.collisions = collisions;
	}
	
	void fillSketches() {
		NodeIterator iter = graph.nodeIterator();
		while ( iter.hasNext() ) {
			int w = iter.nextInt();
			int dw = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				sketches.add( w, v );
			}
		}
	}
	
	public abstract void processArc( int v, int w, int degreev, int degreew );
	
	public void setNumberOfTrianglesForEachNode() {		
		LOGGER.info( "Starting..." );
		long start = System.currentTimeMillis();
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		ExactTriangleCounter exactCounter = new ExactTriangleCounter( graph );
		NodeIterator iter = graph.nodeIterator();
		
		double[] ccExact = null;
		if ( LOGGER.isTraceEnabled() ) {
			ccExact = new double[ graph.numNodes() ];
			DoubleArrays.fill( ccExact, .0 );
		}

		int edges = 0;
		while ( iter.hasNext() ) {
			pl.update();
			int w = iter.nextInt();
			int dw = iter.outdegree();
			int degreew = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				int degreev = graph.outdegree( v );
				// for each arc (w,v)
				if ( w >= v ) continue;
				edges++;
				int exact ;
				if ( LOGGER.isTraceEnabled() ) {
					exact = exactCounter.trianglesInvolvingEdge( w, v );
					ccExact[ w ] += exact;
					ccExact[ v ] += exact;
					setExact( exact );
				}
				processArc( v, w, degreev, degreew );
			}
		}
		
		LOGGER.info( "Edges analyzed " + edges );
		LOGGER.info( "Ending in " + ( System.currentTimeMillis() - start ) + "ms." );
	}
	
	private int exact;
	private void setExact( int exact ){
		this.exact = exact;
	}
	
	public void check( int w, int v, int degreew, int degreev, double estimation ){
		if ( LOGGER.isTraceEnabled() ) {	
			LOGGER.trace( "Triangles involving edge " + w + "," + v + " estimate: " + estimation + " exact: " + exact  + " degree-w: " + degreew + " degree-v: " + degreev );
			if( exact == 0 && estimation!=0 ) LOGGER.error( "Exact is 0 while the contribute of this edge is " + estimation );
			//+ ": "+ "the sketches are " + sketches.get( v ) + " and " + sketches.get( w ) + "; the adj are " + ToString.IntArrayToString( graph.successorArray( v ) ) + " and " + ToString.IntArrayToString( graph.successorArray( w ) ) );
		}
	}
	
}
