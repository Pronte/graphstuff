package it.unimi.di.triangle;

import java.io.IOException;

import it.unimi.di.counter.KBottomCounterArray;
import it.unimi.di.counter.KBottomCounterArray.KBottomCounter;
import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleArrays;
import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.LazyIntIterator;
import it.unimi.dsi.webgraph.NodeIterator;
import it.unimi.dsi.webgraph.Transform;
import it.unipi.di.utils.ToString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;

public class AsymmetricApproximateTriangleCounterBottomK implements TriangleCounter{

	private final static Logger LOGGER = LoggerFactory.getLogger( AsymmetricApproximateTriangleCounterBottomK.class );
	
	private final ImmutableGraph graph;
	int k;
	KBottomCounterArray sketches;
	//KBottomJaccardCounterArray sketches;
	
	double[] ccExact;
	
	
	public AsymmetricApproximateTriangleCounterBottomK( ImmutableGraph graph, int k ){
		this.graph = graph;
		this.k = k;
		sketches = new KBottomCounterArray( k, graph.numNodes(), graph.numNodes() );
		//sketches = new KBottomJaccardCounterArray( k, graph.numNodes(), graph.numNodes() );
		setup();
	}
	
	public void setExact( double[] ccExact ){
		this.ccExact = ccExact;
	}
	
	@Override
	public double[] getNumberOfTrianglesForEachNode() {		
		LOGGER.info( "Starting..." );
		long start = System.currentTimeMillis();
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		ExactTriangleCounter exactCounter = new ExactTriangleCounter( graph );
		NodeIterator iter = graph.nodeIterator();
		double[] cc = new double[ graph.numNodes() ];// this is to maintain the clustering
														// coefficient
		double[] ccExact = null;
		if ( LOGGER.isTraceEnabled() ) {
			ccExact = new double[ graph.numNodes() ];
			DoubleArrays.fill( ccExact, .0 );
		}
		DoubleArrays.fill( cc, .0 );
		int edges = 0;
		//int exactTotal = 0;
		while ( iter.hasNext() ) {
			pl.update();
			int w = iter.nextInt();
			int dw = iter.outdegree();
			int degreew = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				int degreev = graph.outdegree( v );
				// for each arc (w,v)
				if ( w >= v ) continue;
				edges++;
				//long vm = sketches.get( v ).getMaximumRank();
				int vsize = sketches.get( v ).size();
				//long wm = sketches.get( w ).getMaximumRank();
				int wsize = sketches.get( v ).size();
				int inters = (int)sketches.intersection( v, w );
				//System.out.println( "Analyzing edge " + v + "," + w + " with deg " + degreev + " and " + degreew + " inters " + inters );
				double contribute;
				if( vsize < k && wsize<k ) contribute = inters;
				/*else if( vsize <k )	contribute = inters * degreew / k;
				else if( wsize <k ) contribute = inters * degreev / k;
				else if( wm < vm ){
					contribute = degreew * inters / k;
				}else{
					contribute = degreev * inters / k;
				}*/
				else{
					KBottomCounter union = sketches.unionCounter( v, w );
					union.sort();
					KBottomCounter intersection = union.intersectionCounter( sketches.get( v ) );
					intersection.sort();
					double j = intersection.intersection( sketches.get( w ) )/(double)k;
					contribute = ( degreev + degreew )* j /( j + 1 )/2.0;
				}
				
				if ( LOGGER.isTraceEnabled() ) {
					int exact = exactCounter.trianglesInvolvingEdge( w, v );
					//exactTotal += exact;
					ccExact[ w ] += exact;
					ccExact[ v ] += exact;
					LOGGER.trace( "Triangles involving edge " + w + "," + v + " estimate: " + contribute + " exact: " + exact  + " degree-w: " + degreew + " degree-v: " + degreev );
					if( exact == 0 && contribute!=0 ) LOGGER.error( "Exact is 0 while the contribute of this edge is " + contribute + ": "
							+ "the sketches are " + sketches.get( v ) + " and " + sketches.get( w ) + "; the adj are " + ToString.IntArrayToString( graph.successorArray( v ) ) + " and " + ToString.IntArrayToString( graph.successorArray( w ) ) );
				}
				//else LOGGER.debug( "Triangles involving edge " + w + "," + v + " estimate: " + contribute );
				cc[ w ] += contribute;
				cc[ v ] += contribute;
			}
		}
		
		LOGGER.info( "Edges analyzed " + edges );
		LOGGER.info( "Ending in " + ( System.currentTimeMillis() - start ) + "ms." );
		return cc;
	}

	@Override
	public DoubleArrayList getNumberOfTrianglesForEachEdge() {		
		LOGGER.info( "Starting..." );
		long start = System.currentTimeMillis();
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		NodeIterator iter = graph.nodeIterator();
		DoubleArrayList cc = new DoubleArrayList();
		int edges = 0;
		//int exactTotal = 0;
		while ( iter.hasNext() ) {
			pl.update();
			int w = iter.nextInt();
			int dw = iter.outdegree();
			int degreew = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				int degreev = graph.outdegree( v );
				// for each arc (w,v)
				if ( w >= v ) continue;
				edges++;
				//long vm = sketches.get( v ).getMaximumRank();
				int vsize = sketches.get( v ).size();
				//long wm = sketches.get( w ).getMaximumRank();
				int wsize = sketches.get( v ).size();
				int inters = (int)sketches.intersection( v, w );
				//double normalizer = Math.min( vsize, wsize ); // this is k for big neighborhoods
				double contribute;
				//System.out.println( "Analyzing edge " + v + "," + w + " with deg " + degreev + " and " + degreew + " inters " + inters );
				if( vsize < k && wsize<k ) contribute = inters;
				/*else if( vsize <k )	contribute = inters * degreew / normalizer;
				else if( wsize <k ) contribute = inters * degreev / normalizer;
				else if( wm<vm ){
					contribute = degreew * inters / normalizer;
				}else{
					contribute = degreev * inters / normalizer;
				}*/
				else{
					KBottomCounter union = sketches.unionCounter( v, w );
					union.sort();
					KBottomCounter intersection = union.intersectionCounter( sketches.get( v ) );
					intersection.sort();
					double j = intersection.intersection( sketches.get( w ) )/(double)k;
					contribute = ( degreev + degreew )* j /( j + 1 );
				}
				cc.add(contribute);
			}
		}
		LOGGER.info( "Edges analyzed " + edges );
		LOGGER.info( "Ending in " + ( System.currentTimeMillis() - start ) + "ms." );
		return cc;
	}
	
	
	private void setup() {
		NodeIterator iter = graph.nodeIterator();
		while ( iter.hasNext() ) {
			int w = iter.nextInt();
			int dw = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				sketches.add( w, v );
			}
		}
		sketches.sort();
	}

	@Override
	public double getNumberOfTriangles() {
		double[] cc = getNumberOfTrianglesForEachNode();
		double sum = 0;
		for ( int i = 0; i < cc.length; i++ ) {
			sum += cc[ i ];
		}
		sum = sum / 3;
		LOGGER.debug( "Number of Triangles estimated " + sum );
		return sum;
	}

	public static void main( String[] arg ) throws JSAPException, IOException {

		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Compare Exact Method and a method based on K Bottom counters to compute the number of triangles in an undirected graph",
				new Parameter[] {
						new FlaggedOption( "graph", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'g', "graph", "The graph (it will be symmetrized)" ),
						new FlaggedOption( "k", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "sketch size to be used" ),
						new FlaggedOption( "experiments", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed" ),
						new Switch( "symmetrize", 's', "symmetrize", "Symmetrize" ),
				} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		final String g = jsapResult.getString( "graph" );
		final int k = jsapResult.getInt( "k" );
		final int experiments = jsapResult.getInt( "experiments" );

		ImmutableGraph graph = ImmutableGraph.load( g );
		ImmutableGraph graphSymmetric = null;
		if ( jsapResult.getBoolean( "symmetrize" ) ) {
			graphSymmetric = Transform.symmetrize( graph );
			LOGGER.info( "Symmetrizing graph " + g );
		}
		else {
			graphSymmetric = graph;
		}

		runForEachNode( k, experiments, graph, graphSymmetric );
		//runForEachEdge( k, experiments, graph, graphSymmetric );
	}

	public static void runForEachNode( final int k, final int experiments, ImmutableGraph graph, ImmutableGraph graphSymmetric ) {
		SummaryStats[] ss = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] sc = new SummaryStats[ graph.numNodes() ];
		for( int j=0; j<graph.numNodes(); j++ ) ss[j] = new SummaryStats();
		for( int j=0; j<graph.numNodes(); j++ ) sc[j] = new SummaryStats();
		double[] ccExact = new ExactTriangleCounter( graphSymmetric ).getNumberOfTrianglesForEachNode();
		for ( int i = 0; i < experiments; i++ ) {
			AsymmetricApproximateTriangleCounterBottomK approximation = new AsymmetricApproximateTriangleCounterBottomK( graphSymmetric, k );
			double[] cc = approximation.getNumberOfTrianglesForEachNode();
			for( int j=0; j<graph.numNodes(); j++ ){
				if( ccExact[j] != 0 ) ss[j].add( Math.abs( cc[j]-ccExact[j] )/ccExact[j] );
				else if( ccExact[j] == 0 && cc[j] != 0 ) LOGGER.error( "Experiment " + i + " wrong estimation for node " + j  + ": more than 0 triangles " );
				else ss[j].add( 0 );
				
				sc[j].add( cc[j] );
			}
		}
		
		for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "Summary-error: " + j + " " + ss[j].toString() );
		for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "Summary-counters: " + j + " " + sc[j].toString() + " " + ccExact[ j ] );
		
		for( int j=0; j<graph.numNodes(); j++ ) System.out.println( j + " " + sc[j].mean() + " " + ccExact[ j ] );
	}

	public static void runForEachEdge( final int k, final int experiments, ImmutableGraph graph, ImmutableGraph graphSymmetric ) {
		double[] ccExact = new ExactTriangleCounter( graphSymmetric ).getNumberOfTrianglesForEachEdge().toDoubleArray();
		
		SummaryStats[] ss = new SummaryStats[ ccExact.length ];
		SummaryStats[] sc = new SummaryStats[ ccExact.length ];
		for( int j=0; j<ccExact.length; j++ ) ss[j] = new SummaryStats();
		for( int j=0; j<ccExact.length; j++ ) sc[j] = new SummaryStats();
		for ( int i = 0; i < experiments; i++ ) {
			AsymmetricApproximateTriangleCounterBottomK approximation = new AsymmetricApproximateTriangleCounterBottomK( graphSymmetric, k );
			DoubleArrayList cc = approximation.getNumberOfTrianglesForEachEdge();
			int j = 0;
			for( Double h : cc ){
				if( ccExact[j] != 0 ) ss[j].add( Math.abs( h-ccExact[j] )/ccExact[j] );
				else if( ccExact[j] == 0 && h !=0 ) LOGGER.error( "Experiment " + i + " wrong estimation for node " + j  + ": more than 0 triangles " );
				else ss[j].add( 0 );
				
				sc[j].add( h );
				j++;
			}
		}
		
		for( int j=0; j<ss.length; j++ ) LOGGER.info( "Summary-error: " + j + " " + ss[j].toString() );
		for( int j=0; j<sc.length; j++ ) LOGGER.info( "Summary-counters: " + j + " " + sc[j].toString() + " " + ccExact[ j ] );
	}
	
}
