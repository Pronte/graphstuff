#!/bin/bash -e

if [[ "$1" == ""  ]]; then
        echo "usage: $(basename $0) webgraph-basename..." >&2
        exit 1
fi

if [ -z "$CLASSPATH" ]; then
        echo "You need to set the CLASSPATH before running this script!"
        exit 1
fi

base="$1"

java it.unimi.dsi.webgraph.Transform symmetrize $base $base-symmetric
java it.unimi.dsi.webgraph.Transform arcfilter $base-symmetric $base-symmetric-noloops NO_LOOPS

#java it.unimi.dsi.webgraph.BVGraph -o -O -L $base





