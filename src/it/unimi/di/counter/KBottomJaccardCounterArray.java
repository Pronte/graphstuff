package it.unimi.di.counter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.dsi.fastutil.ints.IntArrays;
import it.unimi.dsi.fastutil.longs.LongArrays;
import it.unimi.dsi.fastutil.longs.LongComparator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.util.XorShift1024StarRandom;

public class KBottomJaccardCounterArray extends JaccardCounterArray implements CounterArray {

	private final static Logger LOGGER = LoggerFactory.getLogger( KBottomJaccardCounterArray.class );
	
	//ALERT: this should be LongArrayList
	public int[] rank2Element;
	public int[] element2Rank;
	
	public String KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY= "DensityBasedKBottomCounterJaccardEstimateStrategy";
	
	KBottomCounter[] counters;
	
	final int k;
	final int n;
	
	/*
	int[] invert( int[] a2b ){
		int[] b2a = new int[ a2b.length ];
		for( int a=0; a<a2b.length; a++ ) b2a[ a2b[a] ] = a;
		return b2a;
	}
	*/
	
	
	public KBottomJaccardCounterArray( int k, int m, int n ){
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		element2Rank = new int[ n ];
		for( int i=0; i<n; i++ ) element2Rank[i] = i;
		element2Rank = IntArrays.shuffle( element2Rank , random );
		//int[] rank2Element = invert( element2Rank );
		this.k = k;
		this.n = n;
		counters = new KBottomCounter[ m ];
		for( int i =0; i<counters.length; i++ ){
			counters[ i ] = new KBottomCounter();
		}
	}
	
	public KBottomJaccardCounterArray( int k, int m, int n, String estimationStrategy ){
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		element2Rank = new int[ n ];
		for( int i=0; i<n; i++ ) element2Rank[i] = i;
		element2Rank = IntArrays.shuffle( element2Rank , random );
		//int[] rank2Element = invert( element2Rank );
		this.k = k;
		this.n = n;
		counters = new KBottomCounter[ m ];
		for( int i =0; i<counters.length; i++ ){
			counters[ i ] = new KBottomCounter();
		}
		KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY =  estimationStrategy;
	}
	
	public KBottomCounter get( int i ){
		return counters[ i ];
	}
	
	@Override
	public void add( long a, long element ) {
		counters[ (int)a ].add( element );
	}

	@Override
	public double count( long a ) {
		return counters[ (int)a ].count();
	}

/*
	@Override
	public double union( long a, long b ) {
		KBottomCounter res = counters[(int)a].union( counters[(int)b] );
		return res.count();
	}
	
	@Override
	public double intersection( long a, long b ) {
		KBottomCounter res = counters[(int)a].intersection( counters[(int)b] );
		return res.count();
	}
	*/
	
	@Override
	public double union( long a, long b ) {
		return unionCounter(a,b).size();
	}
	
	@Override
	public double intersection( long a, long b ) {
		return intersectionCounter(a,b).size();
	}
	
	public KBottomCounter unionCounter( long a, long b ) {
		return counters[(int)a].union( counters[(int)b] );
	}

	public KBottomCounter intersectionCounter( long a, long b ) {
		return counters[(int)a].intersection( counters[(int)b] );
	}
	
	
	public double jaccardCoefficient( long a, long b ) {
		return counters[(int)a].jaccardCoefficient( counters[(int)b] );
	}
	
	
	public static LongOpenHashSet intersection( LongOpenHashSet a, LongOpenHashSet b ){
		LongOpenHashSet result = new LongOpenHashSet();
		for( long el : a )
			if( b.contains( el ) ) result.add( el );
		return result ;
	}
	
	public static LongOpenHashSet union( LongOpenHashSet a, LongOpenHashSet b ){
		LongOpenHashSet result = new LongOpenHashSet();
		for( long el : a ) result.add( el );
		for( long el : b ) result.add( el );
		return result ;
	}
	
	public class KBottomCounter{
		
		LongOpenHashSet counter; //Use IntAVLTreeSet
		
		public KBottomCounter(){
			counter = new LongOpenHashSet();
		}

		public KBottomCounter( LongOpenHashSet counter ){
			this.counter = counter;
		}
		
		public String toString(){
			return counter.toString();
		}

		public LongOpenHashSet getCounter(){
			return counter;
		}

		public void add( long element ){
			int i=0;
			
			for ( long a : counter ){
				//ALERT
				if ( element2Rank[ (int)a ] < element ) {
					if( i >=k ) return; 
					else i++;
				}
			}
			counter.add( element );
			
			if(counter.size()>k){
				long max=-1;
				int j=0;
				for ( long a : counter ){
					if( j==0 ) max=a;
					else if( element2Rank[ (int)a ] > element2Rank[ (int)max ] ) max = a;
					j++;
				}
				counter.remove(max);				
			}
		}

		
		public KBottomCounter union( KBottomCounter b ){
			LongOpenHashSet result = new LongOpenHashSet();
			for( long a : counter ) result.add( a );
			for( long a : b.counter ) result.add( a );
			return new KBottomCounter( result );
		}
		
		public KBottomCounter intersection( KBottomCounter b ){
			LongOpenHashSet result = new LongOpenHashSet();
			for( long a : counter )
				if( b.counter.contains( a ) ) result.add( a );
			return new KBottomCounter( result );
		}
		
		
		public int size(){
			return counter.size();
		}
		
		
		public double count(){
			//compute the k-th smallest rank in counter
			long[] sketch = counter.toLongArray();
			if( sketch.length == 0 ) {
				LOGGER.error( "Trying to count for an empty sketch" );
				return 0;
			}
			LOGGER.trace( "Trying to count for sketch " + counter.toString() );
			LongArrays.quickSort( sketch, new LongComparator() {
				
				@Override
				public int compare( Long o1, Long o2 ) {
					return compare( o1.longValue(), o2.longValue() );
				}
				
				@Override
				public int compare( long k1, long k2 ) {
					if( element2Rank[ (int) k1] == element2Rank[ (int) k1] ) return 0;
					else if( element2Rank[ (int) k1] < element2Rank[ (int) k1] ) return -1;
					else return 1;
				}
			} );
			if( k-1 >= sketch.length ) LOGGER.error( "The sketch contains less than " + k + " elements" );
			double tau = element2Rank[ (int)sketch[ Math.min( k-1, sketch.length-1 ) ] ]/(double)n;
			return (k-1)/tau;
		}
		
		public long getElementWithMinimumRank(){
			long min=-1;
			for( long curr : counter ){
				if( min==-1 || element2Rank[(int)curr]<element2Rank[(int)min] ) min=curr;
			}
			return min;
		}
		
		public long getElementWithMaximumRank(){
			long max=-1;
			for( long curr : counter ){
				if( max==-1 || element2Rank[(int)curr]>element2Rank[(int)max] ) max=curr;
			}
			return max;
		}
		
		public long getMaximumRank() {
			long max = getElementWithMaximumRank();
			return element2Rank[(int)max];
		}
				
		public long getElementWithHthRank( int h ){
			
			long[] carr = new long[ counter.size() ];
			counter.toArray( carr );
			
			String arr = "";
			if(LOGGER.isTraceEnabled()) for( int j=0; j<carr.length; j++ ) arr += carr[j]+":"+element2Rank[(int)carr[j]] + " ";
			
			LOGGER.trace( "COUNTER " + arr );
			LongArrays.mergeSort( carr, new LongComparator() {
				
				@Override
				public int compare( Long o1, Long o2 ) {
					return compare( o1.longValue(), o2.longValue() );
				}
				
				@Override
				public int compare( long k1, long k2 ) {
					if( element2Rank[(int)k1] < element2Rank[(int)k2] ) return -1;
					else if ( element2Rank[(int)k1] == element2Rank[(int)k2] ) return 0;
					else return 1;
				}
			} );
			
			arr = "";
			if(LOGGER.isTraceEnabled()) for( int j=0; j<carr.length; j++ ) arr += carr[j]+":"+element2Rank[(int)carr[j]] + " ";
			LOGGER.trace( "COUNTER sorted " + arr );
			LOGGER.trace( "The element with " + h + "-th rank is " + carr[h-1] );
			return carr[h-1];
		}
		
		public LongOpenHashSet getElementsWithRankLessThanOrEqualTo( long th ){
			LongOpenHashSet result = new LongOpenHashSet();
			for( long curr : counter ){
				if( element2Rank[(int)curr]<= th ) result.add( curr );
			}
			return result;
		}
		
		public LongOpenHashSet getElementsWithRankGreaterThanOrLessThanOrEqualTo( long th1, long th2 ){
			LongOpenHashSet result = new LongOpenHashSet();
			for( long curr : counter ){
				if( element2Rank[(int)curr] > th1 && element2Rank[(int)curr]<= th2 ) result.add( curr );
			}
			return result;
		}
		
		
		public LongOpenHashSet getKElementsWithMinimumRank(){
			
			LongOpenHashSet result = new LongOpenHashSet();
			long[] tmp = counter.toLongArray();
			LongArrays.quickSort( tmp, new LongComparator() {
				@Override
				public int compare( Long o1, Long o2 ) {
					return compare( o1.longValue(), o2.longValue() );
				}
				@Override
				public int compare( long k1, long k2 ) {
					return element2Rank[(int)k1] - element2Rank[(int)k2];
				}
			} );
			
			for( int i=0; i<Math.min( k, tmp.length); i++ ){
				result.add( tmp[i] );
			}
			return result;
		}
		
		
		public double jaccardCoefficient( KBottomCounter b ){
			LOGGER.trace( "THIS is :" + this.toString() + " INTERSECTING WITH " + b.toString() );
			KBottomCounter intersection = intersection( b );
			LOGGER.trace( "OBTAINING INTERSECTION:" + intersection.toString() );
			KBottomCounter union = union( b );
			if( KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY.equals( "DensityBasedKBottomCounterJaccardEstimateStrategy" ) ){
				return new DensityBasedKBottomCounterJaccardEstimateStrategy().estimate( this, b, intersection, union );
			}else if( KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY.equals( "RawKBottomCounterJaccardEstimateStrategy" ) ){
				return new RawKBottomCounterJaccardEstimateStrategy().estimate( this, b, intersection, union );				
			}else if( KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY.equals( "ThorupKBottomCounterJaccardEstimateStrategy" ) ){
				return new ThorupKBottomCounterJaccardEstimateStrategy().estimate( this, b, intersection, union );				
			}else if( KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY.equals( "IncrementalKBottomCounterJaccardEstimateStrategy" ) ){
				return new IncrementalKBottomCounterJaccardEstimationStrategy().estimate( this, b, intersection, union );				
			}
			LOGGER.error( "ERROR: no estimation strategy is given for " + KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY );
			return -1;
		}

	}	
	
	
	public interface KBottomCounterJaccardEstimateStrategy{
		double estimate( KBottomCounter a, KBottomCounter b, KBottomCounter intersection, KBottomCounter union );
	}
	
	public class DensityBasedKBottomCounterJaccardEstimateStrategy implements KBottomCounterJaccardEstimateStrategy{
		public double estimate( KBottomCounter a, KBottomCounter b, KBottomCounter intersection, KBottomCounter union ) {
			//LOGGER.debug( "SKETCH UNION: " + union.counter.size() + " SKETCH INTERSECTION: "+intersection.counter.size() );
			if(union.counter.size()!=0) LOGGER.trace( "SKETCH UNION " + union.counter.size() + " with rank minimum: " + element2Rank[(int)union.getElementWithMinimumRank()] + " and maximum: " + element2Rank[(int)union.getElementWithMaximumRank()] );
			if(intersection.counter.size()!=0)	LOGGER.trace( "SKETCH INTERSECTION "+intersection.counter.size() + " with rank minimum: " + element2Rank[(int)intersection.getElementWithMinimumRank()] + " and maximum: " + element2Rank[(int)intersection.getElementWithMaximumRank()] );
			
			if( a.counter.size() == 0 || b.counter.size() == 0 ) return 0;
			
			long bmax = element2Rank[(int)b.getElementWithMaximumRank()];
			long bmin = element2Rank[(int)b.getElementWithMinimumRank()];
			long amax = element2Rank[(int)a.getElementWithMaximumRank()];
			long amin = element2Rank[(int)a.getElementWithMinimumRank()];
			//long den = Math.max( amax, bmax ) - Math.min( amin, bmin ) +1;
			LOGGER.trace( "A with rank minimum: " + amin + " and maximum: " + amax );
			LOGGER.trace( "B with rank minimum: " + bmin + " and maximum: " + bmax );
			if( a.counter.size() <k ) amax = n;
			if( b.counter.size() <k ) bmax = n;
			long th = Math.min( amax, bmax );
			
			LongOpenHashSet aaa = a.getElementsWithRankLessThanOrEqualTo( th );
			LOGGER.trace( "Elements of A with rank <= " + th + ": " + aaa.toString()  );
			LongOpenHashSet bbb = b.getElementsWithRankLessThanOrEqualTo( th );
			LOGGER.trace( "Elements of B with rank <= " + th + ": " + bbb.toString()  );
			
			long den = KBottomJaccardCounterArray.union( aaa, bbb ).size();
			long num = KBottomJaccardCounterArray.intersection( aaa, bbb ).size();
			
			LOGGER.trace( "COMPUTING: num: " + num + " den: " +den);
			return num/(double)den;
		}
	}
	
	public class IncrementalKBottomCounterJaccardEstimationStrategy implements KBottomCounterJaccardEstimateStrategy{

		@Override
		public double estimate( KBottomCounter a, KBottomCounter b, KBottomCounter intersection, KBottomCounter union ) {
			double partial = -1;
			long th = -1;
			for( int h=1; h<=k; h++ ){
				long bmax = b.counter.size() < h ? n : element2Rank[(int)b.getElementWithHthRank( h )];
				long amax =  a.counter.size() < h ? n : element2Rank[(int)a.getElementWithHthRank( h )];
				long newth = Math.min( amax, bmax );
				LongOpenHashSet aaa = a.getElementsWithRankGreaterThanOrLessThanOrEqualTo( th, newth );
				LOGGER.trace( "Elements of A with rank > " + th + " and <= " + newth + ": " + aaa.toString()  );
				LongOpenHashSet bbb = b.getElementsWithRankGreaterThanOrLessThanOrEqualTo( th, newth );
				LOGGER.trace( "Elements of A with rank > " + th + " and <= " + newth + ": " + bbb.toString()  );
				
				long den = KBottomJaccardCounterArray.union( aaa, bbb ).size();
				long num = KBottomJaccardCounterArray.intersection( aaa, bbb ).size();
				
				LOGGER.trace( "COMPUTING: num: " + num + " den: " +den);
				LOGGER.trace( "partial=" + partial + " union/intersection " + num/(double)den );
				partial = ((h-1)/(double)h) * partial + (1./h) * num/(double)den;
				th = newth;
			}
			return partial;
		}
		
	}
	
	public class RawKBottomCounterJaccardEstimateStrategy implements KBottomCounterJaccardEstimateStrategy{
		public double estimate( KBottomCounter a, KBottomCounter b, KBottomCounter intersection, KBottomCounter union ) {
			if(union.size()==0) return 0;
			return intersection.size()/(double)union.size();
		}
	}
	
	public class ThorupKBottomCounterJaccardEstimateStrategy implements KBottomCounterJaccardEstimateStrategy{
		public double estimate( KBottomCounter a, KBottomCounter b, KBottomCounter intersection, KBottomCounter union ) {
			if(union.size()==0) return 0;
			LongOpenHashSet kunion = union.getKElementsWithMinimumRank();
			KBottomCounter tmp = intersection.intersection( new KBottomCounter( kunion ) );
			return tmp.size()/(double)kunion.size();
		}
	}

}

