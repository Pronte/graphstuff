#!/bin/bash -e

if [[ "$1" == ""  ]]; then
        echo "usage: $(basename $0) graphBaseName url..." >&2
        exit 1
fi

if [ -z "$CLASSPATH" ]; then
        echo "You need to set the CLASSPATH before running this script!"
        exit 1
fi

base="$1"
url="$2"
wget $url -O $base-o 
unzip $base-o

grep -v "#" $base-o | tail -n +2 | sort -n | uniq  > $base-s

java it.unimi.dsi.webgraph.BVGraph -g ArcListASCIIGraph $base-s $base

#java it.unimi.dsi.webgraph.BVGraph -o -O -L $base





