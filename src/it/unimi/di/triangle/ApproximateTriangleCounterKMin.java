package it.unimi.di.triangle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;

import it.unimi.di.counter.DensityKBottomCounterArray;
import it.unimi.di.counter.KMins;
import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.fastutil.doubles.DoubleArrays;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.Transform;


public class ApproximateTriangleCounterKMin extends ApproximateTriangleCounter {
	
	private final static Logger LOGGER = LoggerFactory.getLogger( ApproximateTriangleCounterKMin.class );
	double[] ccKMin;
	
	public ApproximateTriangleCounterKMin( ImmutableGraph graph, int k, int seed, boolean collisions ){
		super( graph, k, seed, collisions );
		setup();
	}
	
	private void setup() {
		sketches = new KMins( k, graph.numNodes(), graph.numNodes(), collisions, seed );
		fillSketches();
		sketches.prepare();
		ccKMin = new double[ graph.numNodes() ];
		DoubleArrays.fill( ccKMin, .0 );
	}
	
	public void processArc( int v, int w, int degreev, int degreew ){
		double contributeKMin;

		double jac = sketches.jaccardCoefficient( v, w );
		contributeKMin = ( degreev + degreew )* jac /( jac + 1 )/2.0;
		
		//else LOGGER.debug( "Triangles involving edge " + w + "," + v + " estimate: " + contribute );
		ccKMin[ w ] += contributeKMin;
		ccKMin[ v ] += contributeKMin;
		
		if ( LOGGER.isTraceEnabled() ){
			check( v, w, degreev, degreew, contributeKMin );
		}
	}
	
	public static void main( String[] arg ) throws JSAPException, IOException {

		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Execute a method based on K Min counters to compute the number of triangles in an undirected graph",
				new Parameter[] {
						new FlaggedOption( "graph", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'g', "graph", "The graph (it will be symmetrized)" ),
						new FlaggedOption( "k", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "sketch size to be used" ),
						new FlaggedOption( "experiments", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed" ),
						new FlaggedOption( "output", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'o', "output", "The file name for the output" ),
						new Switch( "symmetrize", 'y', "symmetrize", "Symmetrize" ),
						new Switch( "collision", 'l', "collision", "Allow collisions" ),
						new FlaggedOption( "seed", JSAP.INTEGER_PARSER, Integer.toString(DEFAULT_SEED), JSAP.NOT_REQUIRED, 's', "seed", "seed" )
				} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		final String g = jsapResult.getString( "graph" );
		final int k = jsapResult.getInt( "k" );
		final int experiments = jsapResult.getInt( "experiments" );
		final String file1 = jsapResult.getString( "output" );
		int seed = jsapResult.getInt("seed");
		boolean collisions = jsapResult.getBoolean( "collision" );
		
		ImmutableGraph graph = ImmutableGraph.load( g );
		ImmutableGraph graphSymmetric = null;
		if ( jsapResult.getBoolean( "symmetrize" ) ) {
			graphSymmetric = Transform.symmetrize( graph );
			LOGGER.info( "Symmetrizing graph " + g );
		}
		else {
			graphSymmetric = graph;
		}
		runForEachNode( k, experiments, graph, graphSymmetric, file1, collisions, seed );
	}
	
	public static void runForEachNode( final int k, final int experiments, ImmutableGraph graph, ImmutableGraph graphSymmetric, String fileName1, boolean collisions, int seed ) throws FileNotFoundException {
		SummaryStats[] ssKMin = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] scKMin = new SummaryStats[ graph.numNodes() ];
		for( int j=0; j<graph.numNodes(); j++ ) {
			ssKMin[j] = new SummaryStats();
			scKMin[j] = new SummaryStats();
		}
		for ( int i = 0; i < experiments; i++ ) {
			ApproximateTriangleCounterKMin approximation = new ApproximateTriangleCounterKMin( graphSymmetric, k, seed, collisions );
			approximation.setNumberOfTrianglesForEachNode();
			for( int j=0; j<graph.numNodes(); j++ ){
				scKMin[j].add( approximation.ccKMin[j] );
			}
		}
		if( experiments!=1 ){
			for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "KMin-Summary-counters: " + j + " " + scKMin[j].toString() );
		}

		
		PrintStream out1 = new PrintStream( fileName1 );
		
		for( int j=0; j<graph.numNodes(); j++ ) out1.println( j + " " + scKMin[j].mean() );
		out1.close();
	}
	
	
	

}
