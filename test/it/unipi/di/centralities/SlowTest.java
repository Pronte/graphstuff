package it.unipi.di.centralities;

import it.unimi.dsi.webgraph.ArrayListMutableGraph;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unipi.di.centralities.Slow;

public class SlowTest {

	public static void test1(){
		ImmutableGraph graph = new ArrayListMutableGraph( 9, new int[][] {
				{0,1},
				{1,2},
				{2,8},
				{4,5},
				{5,0},{5,1},{5,3},{5,4},{5,6},
				{6,7},
				{7,7},{7,8}
		} ).immutableView();
		new Slow( graph, 11 ).run();
	}
	
	public static void test2(){
		ImmutableGraph graph = new ArrayListMutableGraph( 4, new int[][] {
				{0,1},
				{0,2},
				{1,3},
				{2,3}
		} ).immutableView();
		new Slow( graph, 3 ).run();
	}
	
	public static void test3(){
		ImmutableGraph graph = new ArrayListMutableGraph( 3, new int[][] {
				{0,1},
				{1,2}
		} ).immutableView();
		new Slow( graph, 3 ).run();
	}
	
	
	public static void main( String[] args ){
		test3();
	}
	
	
}
