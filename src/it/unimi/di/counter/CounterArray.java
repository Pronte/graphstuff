package it.unimi.di.counter;

public interface CounterArray {

	void add( long a, long element );

	double count( long a );

	double union( long a, long b );
	
	double intersection( long a, long b );

}
