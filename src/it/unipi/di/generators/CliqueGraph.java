package it.unipi.di.generators;

public class CliqueGraph extends GraphGenerator {

	public CliqueGraph( int n ){
		this.nodes = n;
		
		result = new int[n * n - n][];
		for( int i=0; i<n; i++ ){
			for ( int j=0; j<n; j++ ){
				if ( i > j ) result[ i * (n-1) + j ] = new int[]{ i, j };
				else if ( i< j ) result[ i * (n-1) + j-1 ] = new int[]{ i, j };
			}
		}
	}
	
}
