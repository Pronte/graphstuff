package it.unipi.di.utils;

public class ToString {

	public static String IntArrayToString( int[] a ){
		
		String rst = "";
		for( int i=0; i< a.length; i++ )
			rst += a[i] + " ";
		return rst.trim();
	}
	
}
