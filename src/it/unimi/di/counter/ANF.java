package it.unimi.di.counter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.dsi.big.webgraph.ImmutableGraph;
import it.unimi.dsi.big.webgraph.LazyLongIterator;
import it.unimi.dsi.big.webgraph.NodeIterator;
import it.unimi.dsi.logging.ProgressLogger;


public class ANF {

	ImmutableGraph graph;
	LogCounterArray[] counterArray;
	private final static Logger LOGGER = LoggerFactory.getLogger( ANF.class );
	
	public ANF( ImmutableGraph graph, int m, int r ){
		this.graph = graph;
		counterArray = new LogCounterArray[ graph.intNumNodes() ];
		for( int i = 0; i<graph.intNumNodes() ; i++ ){
			counterArray[i] = new LogCounterArray( m, r );
			counterArray[i].init();
		}
	}
	
	public void run( int limit ){
		
		LOGGER.info( "Starting..." );
		long start = System.currentTimeMillis();
		
		for( int i = 0; i< limit; i++){
			int edges = 0;
			ProgressLogger pl = new ProgressLogger();
			pl.expectedUpdates = graph.numNodes();
			pl.itemsName = "nodes";
			
			LogCounterArray[] newCounterArray = new LogCounterArray[ counterArray.length ];
			for( int j=0; j<counterArray.length; j++ ) newCounterArray[ j ] = counterArray[j].copy();
			
			NodeIterator iter = graph.nodeIterator();
			while( iter.hasNext() ){
				pl.update();
				long w=iter.next();
				long dw = iter.outdegree();	
				LazyLongIterator successors = iter.successors();
				while( dw-- != 0 ) {
					long v=successors.nextLong();
					//for each arc (w,v)
					newCounterArray[ (int)w ] = newCounterArray[ (int)w ].or( counterArray[(int)v] );
					edges ++;
				}
			}
			LOGGER.info( "Edges analyzed " + edges );
			
			counterArray = newCounterArray;
			emit();
		}
		LOGGER.info( "Ending in " + ( System.currentTimeMillis() - start ) + "ms." );
	}

	private void emit() {
		for( int i=0; i<counterArray.length; i++ )
			LOGGER.info( i + ": " + counterArray[ i ].count());
	}
	
}
