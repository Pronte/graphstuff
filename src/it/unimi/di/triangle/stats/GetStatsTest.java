package it.unimi.di.triangle.stats;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.martiansoftware.jsap.JSAPException;


public class GetStatsTest {
	
	private static String tf = "_target.txt";
	private static String f1 = "_file1.txt";
	private static String f2 = "_file2.txt";
	
	
	private static String targetContent = ""
			+ "0 2.0\n" +
			"1 1.0\n" +
			"2 2.0\n" +
			"3 0.0\n" +
			"4 64.0\n" +
			"5 1.0\n" +
			"6 6.0\n" +
			"7 6.0\n" +
			"8 25.0\n" +
			"9 8.0";
	private static String targetContentPerm = ""+
			"7 6.0\n" +
			"0 2.0\n" +
			"4 64.0\n" +
			"1 1.0\n" +
			"9 8.0\n"+
			"2 2.0\n" +
			"3 0.0\n" +
			"5 1.0\n" +
			"6 6.0\n" +
			"8 25.0";
	
	private static String targetContentToy = ""
			+ "0 2.0\n" +
			"1 1.0\n" ;
	
	private static String targetContentPermToy = ""+
			"1 1.0\n" +
			"0 2.0\n" ;
	
	private static String file1ContentG1Toy = ""+
			"0 10.800904977375565\n"+
			"1 0.7777777777777778";
	private static String file2ContentG1Toy = ""+
			"0 1.599009900990099\n"+
			"1 0.8666666666666667";
	
	
	private static String file1ContentG1 = ""+
			"0 10.800904977375565\n"+
			"1 0.7777777777777778\n"+
			"2 1.4316239316239319\n"+
			"3 0.0\n"+
			"4 53.0685586515308\n"+
			"5 0.0\n"+
			"6 4.129049773755656\n"+
			"7 7.494299408999954\n"+
			"8 20.97598899208806\n"+
			"9 7.710340794515023\n";
	private static String file2ContentG1 = "0 1.599009900990099\n"+
			"1 0.8666666666666667\n"+
			"2 1.4656765676567658\n"+
			"3 0.0\n"+
			"4 70.43467214953354\n"+
			"5 1.363260619977038\n"+
			"6 6.990412892336552\n"+
			"7 5.930137932981576\n"+
			"8 24.453073286614682\n"+
			"9 8.263555786693587";
	
	private static String file1ContentG2 ="0 3.0\n"+
			"1 2.0\n"+
			"2 3.0\n"+
			"3 0.0\n"+
			"4 95.72368797485768\n"+
			"5 1.669230769230769\n"+
			"6 12.0\n"+
			"7 12.0\n"+
			"8 45.95081585081585\n"+
			"9 14.692307692307692\n";

	private static String file2ContentG2 ="0 3.0\n"+
			"1 2.0\n"+
			"2 3.0\n"+
			"3 0.0\n"+
			"4 92.4541586156771\n"+
			"5 1.669230769230769\n"+
			"6 12.0\n"+
			"7 12.0\n"+
			"8 45.28787878787879\n"+
			"9 14.692307692307692\n";
	
	public static void test1Toy() throws IOException, JSAPException{
		
		FileUtils.writeStringToFile(new File( tf ), targetContentToy );
		FileUtils.writeStringToFile(new File( f1 ), file1ContentG1Toy );
		FileUtils.writeStringToFile(new File( f2), file2ContentG1Toy );
		
		GetStatDistribution.main( ("-t "+tf+" "+f1+","+f2).split( " " ) );
		
		new File( tf ).delete();
		new File( f1 ).delete();
		new File( f2 ).delete();
	}
	
	public static void test1PermToy() throws IOException, JSAPException{
		
		FileUtils.writeStringToFile(new File( tf ), targetContentPermToy );
		FileUtils.writeStringToFile(new File( f1 ), file1ContentG1Toy );
		FileUtils.writeStringToFile(new File( f2), file2ContentG1Toy );
		
		GetStatDistribution.main( ("-t "+tf+" "+f1+","+f2).split( " " ) );
		
		new File( tf ).delete();
		new File( f1 ).delete();
		new File( f2 ).delete();
	}
	
	public static void test1() throws IOException, JSAPException{
		
		FileUtils.writeStringToFile(new File( tf ), targetContent );
		FileUtils.writeStringToFile(new File( f1 ), file1ContentG1 );
		FileUtils.writeStringToFile(new File( f2), file2ContentG1 );
		
		GetStatDistribution.main( ("-t "+tf+" "+f1+","+f2).split( " " ) );
		
		new File( tf ).delete();
		new File( f1 ).delete();
		new File( f2 ).delete();
	}
	
	public static void test2() throws IOException, JSAPException{
		
		FileUtils.writeStringToFile(new File( tf ), targetContent );
		FileUtils.writeStringToFile(new File( f1 ), file1ContentG2 );
		FileUtils.writeStringToFile(new File( f2), file2ContentG2 );
		
		GetStatDistribution.main( ("-t "+tf+" "+f1+","+f2).split( " " ) );
		
		new File( tf ).delete();
		new File( f1 ).delete();
		new File( f2 ).delete();
	}
	
	public static void test1Perm() throws IOException, JSAPException{
		
		FileUtils.writeStringToFile(new File( tf ), targetContentPerm );
		FileUtils.writeStringToFile(new File( f1 ), file1ContentG1 );
		FileUtils.writeStringToFile(new File( f2), file2ContentG1 );
		
		GetStatDistribution.main( ("-t "+tf+" "+f1+","+f2).split( " " ) );
		
		new File( tf ).delete();
		new File( f1 ).delete();
		new File( f2 ).delete();
	}
	
	public static void test2Perm() throws IOException, JSAPException{
		
		FileUtils.writeStringToFile(new File( tf ), targetContentPerm );
		FileUtils.writeStringToFile(new File( f1 ), file1ContentG2 );
		FileUtils.writeStringToFile(new File( f2), file2ContentG2 );
		
		GetStatDistribution.main( ("-t "+tf+" "+f1+","+f2).split( " " ) );
		
		new File( tf ).delete();
		new File( f1 ).delete();
		new File( f2 ).delete();
	}
	
	
	public static void testPermuted() throws IOException, JSAPException{
		System.out.println( "TEST 1______________________________________" );
		test1();
		System.out.println( "TEST 1 PERMUTED______________________________________" );
		test1Perm();
		System.out.println( "TEST 2______________________________________" );
		test2();
		System.out.println( "TEST 2 PERMUTED______________________________________" );
		test2Perm();
	}
	
	public static void main( String[] args ) throws IOException, JSAPException{
		test1();
		test2();
		
	}
	
	
	
	
	
	
}
