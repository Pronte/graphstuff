package it.unimi.di.counter;

import it.unimi.dsi.fastutil.ints.IntArrays;
import it.unimi.dsi.fastutil.longs.LongArrays;
import it.unimi.dsi.util.XorShift1024StarRandom;

public class KPartitionCounterArray extends JaccardCounterArray implements CounterArray{

	KPartitionCounter[] counters;
	int m;
	int n;
	int k;
	int[] element2Partition;
	int[] element2Rank;
	
	public static final boolean EXPONENTIAL_PARTITION = false;
	
	
	public KPartitionCounterArray( int k, int m, int n ){
		this.k=k;
		this.m=m;
		this.n=n;
		
		counters = new KPartitionCounter[m];
		for( int i=0; i<m; i++ ) counters[i] = new KPartitionCounter( k );
		
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		element2Rank = new int[ n ];
		for( int i=0; i<n; i++ ) element2Rank[i] = i;
		element2Rank = IntArrays.shuffle( element2Rank , random );
		element2Partition = new int[n];
		for( int i=0; i<n; i++ ){
			if( !EXPONENTIAL_PARTITION ){
				int coin = random.nextInt( k );
				element2Partition[ i ] = coin;
			}else element2Partition[ i ] = getPartition( i, random, k);
		}
	}
	
	
	//return i with probability 1/2^(i+1) and at maximum k-1
	private int getPartition( int i, XorShift1024StarRandom random, int k){
		for( int j=0; j<k; j++){
			double coin = random.nextDouble();
			if( coin < 0.5) return j;
		}
		return k-1;
	}
	
	@Override
	public double jaccardCoefficient( long a, long b ) {
		return counters[(int)a].jaccardCoefficient( counters[(int)b] );
	}

	@Override
	public void add( long a, long element ) {
		counters[(int)a].add(element);
	}

	@Override
	public double count( long a ) {
		//throw new UnsupportedOperationException();
		return -1;
	}

	@Override
	public double union( long a, long b ) {
		//throw new UnsupportedOperationException();
		return -1;
	}

	@Override
	public double intersection( long a, long b ) {
		//throw new UnsupportedOperationException();
		return -1;
	}
	
	
	public class KPartitionCounter{

		long[] min;
		
		public KPartitionCounter( int k ) {
			min = new long[ k ];
			LongArrays.fill( min, -1 );
		}

		public void add( long element ) {
			int partition = element2Partition[ (int) element ];
			if( min[partition] == -1) min[partition]=element;
			else if( element2Rank[ (int)min[partition] ] > element2Rank[ (int)element ] ) min[partition]=element;
		}

		public double jaccardCoefficient( KPartitionCounter pc ) {
			double result = 0;
			int unsetA = 0;
			int unsetB = 0;
			int intersection = 0;
			for( int i=0; i<min.length; i++ ){
				double weight =0;
				if( !EXPONENTIAL_PARTITION ) weight=n/k;//since it is uniform	
				else{
					weight = n / Math.pow( 2, i+1 );
					if( i== min.length-1 ) weight*=2;	
				}
				if( min[i]!=-1 ){
					if( min[i] == pc.min[i] ) {
						result += weight;
						intersection++;
					}
				} else unsetA++;
				if( pc.min[i] == -1 ) unsetB++;
			}
			result /= n;
			
			if( unsetA ==k || unsetB ==k ) return 0;
			if( !EXPONENTIAL_PARTITION ) result = (double)intersection / Math.max( k-unsetA, k-unsetB );
			return result;
		}
		
		public double jaccardCoefficientOld( KPartitionCounter pc ) {
			double result = 0;
			int unsetA = 0;
			int unsetB = 0;
			int intersection = 0;
			for( int i=0; i<min.length; i++ ){
				double weight =0;
				if( !EXPONENTIAL_PARTITION ) weight=n/k;//since it is uniform	
				else{
					weight = n / Math.pow( 2, i+1 );
					if( i== min.length-1 ) weight*=2;	
				}
				if( min[i]!=-1 ){
					if( min[i] == pc.min[i] ) {
						result += weight;
						intersection++;
					}
				} else unsetA++;
				if( pc.min[i] == -1 ) unsetB++;
			}
			result /= n;
			
			if( unsetA ==k || unsetB ==k ) return 0;
			if( !EXPONENTIAL_PARTITION ) result = (double)intersection / Math.max( k-unsetA, k-unsetB );
			return result;
		}
		
	}
	
	
	
}
