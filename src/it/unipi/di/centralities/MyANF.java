package it.unipi.di.centralities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyANF {

	// #vertex #mask #bit
	boolean[][][] masks;
	boolean undir;
	boolean cont;
	int n;
	int m; // number of mask
	int masklength;
	String inputfile;
	int limit;
	long[] modifiedBetweenness;
	// Given i,j,k, such that there are r shortest paths from i to j, and the
	// shortest path from i to k passes through j, j has modifiedBetweenness 1

	double[] bitprob;

	public MyANF(String inputfile, int limit, int m, boolean und)
			throws IOException {
		this.inputfile = inputfile;
		File finput = new File(inputfile);
		BufferedReader br = new BufferedReader(new FileReader(finput));
		String line = br.readLine();// number of nodes
		while (line.startsWith("# ")) {
			line = br.readLine();
		}
		// cerca max
		n = -1;
		while (line != null) {
			String[] edge = line.split(" ");
			int u = Integer.parseInt(edge[0].trim());
			int v = Integer.parseInt(edge[1].trim());
			if (u >= n) {
				n = u + 1;
			}
			if (v >= n) {
				n = v + 1;
			}
			line = br.readLine();
		}
		br.close();

		undir = und;
		this.m = m;
		masklength = (int) (Math.log(n) + 7);
		masks = new boolean[n][m][masklength];

		bitprob = new double[masklength];
		for (int i = 0; i < bitprob.length; i++) {
			bitprob[i] = 1 / Math.pow(2, i + 1);
		}

		for (int j = 0; j < m; j++) {
			init(j);
		}
		cont = true;

		this.limit = limit;

		modifiedBetweenness = new long[n];
		for (int i = 0; i < modifiedBetweenness.length; i++) {
			modifiedBetweenness[i] = 0;
		}
	}

	public boolean[] or(boolean[] input1, boolean[] input2) {
		if (input1.length != input2.length) {
			throw new RuntimeException();
		}
		boolean rst[] = new boolean[input1.length];
		for (int i = 0; i < input1.length; i++) {
			if (input1[i] || input2[i] != rst[i]) {
				cont = true;
			}
			rst[i] = input1[i] || input2[i];
		}
		return rst;
	}

	private void init(int nmask) {

		for (int v = 0; v < n; v++) {
			double p = Math.random();
			int rb = -1;
			double sum = 0;
			for (int i = 0; i < bitprob.length; i++) {
				sum += bitprob[i];
				if (p < sum) {
					rb = i;
					break;
				}
			}
			if (rb == -1) {
				rb = bitprob.length - 1;
			}
			masks[v][nmask][rb] = true;
		}
		/*
		 * int[] asd=new int[n]; for(int i=0; i<masks.length; i++){ boolean
		 * sw=false; for(int z=0; z<masks[0][0].length; z++){
		 * if(masks[i][nmask][z]==true){ if(sw==false){ asd[z]++; sw=true;
		 * }else{System.out.println("ERRORE");} } } if(sw==false){
		 * System.out.println("ERRORE: Node "+i+" without mask"); } }
		 * MyUtils.print(asd); System.out.println();
		 */
	}

	private boolean[][][] copy(boolean[][][] input) {
		boolean rst[][][] = new boolean[input.length][input[0].length][input[0][0].length];
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[0].length; j++) {
				for (int z = 0; z < input[0][0].length; z++) {
					rst[i][j][z] = input[i][j][z];
				}
			}
		}
		return rst;
	}

	private boolean[][] or(boolean[][] a, boolean[][] b) {
		boolean[][] rst = new boolean[a.length][a[0].length];
		for (int mk = 0; mk < m; mk++) {
			rst[mk] = or(a[mk], b[mk]);
		}
		return rst;
	}

	public void run() throws IOException {
		double est;
		for (int h = 0; h < limit - 1; h++) {
			boolean[][][] oldmasks = copy(masks);

			File finput = new File(inputfile);
			BufferedReader br = new BufferedReader(new FileReader(finput));
			String line = br.readLine();
			while (line.startsWith("# ")) {
				line = br.readLine();
			}
			// edges
			while (line != null) {
				String[] edge = line.split(" ");
				int u = Integer.parseInt(edge[0].trim());
				int v = Integer.parseInt(edge[1].trim());

				masks[u] = or(masks[u], oldmasks[v]);

				boolean[][] tmp = or(oldmasks[u], oldmasks[v]);

				if (!undir) {
					modifiedBetweenness[v] += estimate(tmp)
							- estimate(oldmasks[u]);
				} else {
					// WARNING: if (u,v) in the file then (v,u) have not to be
					// in the file
					masks[v] = or(masks[v], oldmasks[u]);
					modifiedBetweenness[v] += estimate(tmp)
							- estimate(oldmasks[u]);
					modifiedBetweenness[u] += estimate(tmp)
							- estimate(oldmasks[v]);
				}

				line = br.readLine();
			}
			br.close();
			est = estimate();
			System.out.println(est);
		}

		int[] seq = new int[n];
		for (int i = 0; i < n; i++) {
			seq[i] = i;
		}

		/*
		MyUtils.print(seq);
		System.out.println();
		System.out.println("ANF Total: " + MyUtils.getSum(modifiedBetweenness));
		MyUtils.print(modifiedBetweenness);
		*/

	}

	private double estimate() {
		double sum = 0;
		for (int i = 0; i < masks.length; i++) {
			sum += estimate(i);
		}
		return sum;
	}

	private double estimate(int v) {

		return estimate(masks[v]);
		/*
		 * double averageb=0; for(int j=0; j<masks[0].length; j++){ int z=0;
		 * for(z=0; z<masks[0][0].length; z++){ if(masks[v][j][z]==false) break;
		 * } averageb+=z; } averageb=averageb/masks[0].length; return
		 * Math.pow(2, averageb)/0.77351;
		 */
	}

	private double estimate(boolean[][] mk) {
		double averageb = 0;
		for (int j = 0; j < mk.length; j++) {
			int z = 0;
			for (z = 0; z < mk[0].length; z++) {
				if (mk[j][z] == false)
					break;
			}
			averageb += z;
		}
		averageb = averageb / mk.length;
		return Math.pow(2, averageb) / 0.77351;
	}

	public static void main(String[] args) throws IOException {
		String inputfile = "yeastInter.txt";
		(new MyANF(inputfile, 10, 1024, true)).run();
	}

}
