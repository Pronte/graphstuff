package it.unimi.di.triangle.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import it.unimi.di.triangle.ApproximateTriangleCounterBottomK;
import it.unimi.di.triangle.AsymmetricApproximateTriangleCounterBottomK;
import it.unimi.di.triangle.ExactTriangleCounter;
import it.unimi.di.triangle.stats.GetStatDistribution;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.webgraph.ArrayListMutableGraph;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.Transform;
import it.unipi.di.generators.ErdosRenyiGraph;
import it.unipi.di.generators.GraphGenerator;

import org.junit.Assert;

import com.martiansoftware.jsap.JSAPException;

public class TriangleCounterTest {
	
	private static final int N = 10;
	private static final int M = 20;
	private final static int EXPERIMENTS = 2000;
	private final static int K = 512;
	
	public static void test1() throws JSAPException, IOException{
		
		ErdosRenyiGraph ttt= new ErdosRenyiGraph( N, M, 131 );
		testTriangleForEachNode( ttt );
	}
	
	
	private static void testTriangleForEachNode( GraphGenerator ttt ) throws JSAPException, IOException{
		ImmutableGraph graph = new ArrayListMutableGraph( ttt.getNumberOfNodes(), ttt.getGraph() ).immutableView();		
		ImmutableGraph graph1 = Transform.symmetrize( graph );
		ImmutableGraph graph2 = Transform.filterArcs( graph1, Transform.NO_LOOPS );
		SummaryStats ss = new SummaryStats();
		
		new ExactTriangleCounter( graph2 ).getNumberOfTrianglesForEachNode( "_file-target" );
		System.out.print( "Exact computed" );
		ApproximateTriangleCounterBottomK.runForEachNode( K, 1, graph2, "_file1", "_file2", false, 131 );
		System.out.print( "KBottom done" );
		new GetStatDistribution().main( "-t _file-target _file1,_file2".split( " " ) );
		//new File( "_file-target" ).delete();
		//new File( "_file1" ).delete();
		//new File( "_file2" ).delete();
		
	}
	
	public static void main( String[] args ) throws JSAPException, IOException{
		test1();
	}
	
	
	

}
