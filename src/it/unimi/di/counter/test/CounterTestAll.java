package it.unimi.di.counter.test;

import java.util.Date;

import it.unimi.di.counter.CounterArray;
import it.unimi.di.counter.JaccardCounterArray;
import it.unimi.di.counter.JaccardKMins;
import it.unimi.di.counter.KBottomJaccardCounterArray;
import it.unimi.di.counter.KPartitionCounterArray;
import it.unimi.dsi.fastutil.booleans.BooleanArrays;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.util.XorShift1024StarRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;

public class CounterTestAll {
	
	private final static Logger LOGGER = LoggerFactory.getLogger( CounterTest.class );
	private final static boolean DEBUG_ENABLED = true;
	private final static boolean PRINT_SIZE = true;
	private static final boolean TEST_JACCARD = true;
	private static final String[] STRATEGIES = { "KBottomDensity", "KBottomThorup", "KMins", "KPartition" };
	
	private static SummaryStats[] jaccardGlobalErrors;
	
	private static int infiniteJaccardError=0;
	

	private static int n = -1;
	private static int k = -1;
	private static int item = -1;
	private static int experiments = -1;
	private static double step = -1;
	private static int repeat = -1;
	
	
	public static void main( String[] arg ) throws JSAPException{
		
		Date dt = new Date();
		LOGGER.info( dt.toString() + " Starting Experiment..." );
		System.out.println( dt.toString() + " Starting Experiment..." );
		SimpleJSAP jsap = new SimpleJSAP( CounterTest.class.getName(), "Test Counters, by assigning elements of a universe to two sets A and B, by varying their probability to get elements",
				new Parameter[] {
					new FlaggedOption( "u", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'u', "universesize", "The size of the universe" ),
					new FlaggedOption( "k", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "The logarithm of the number of registers (log2m for HyperLogLog) or k (Cohen)" ),
					new FlaggedOption( "i", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'i', "item", "The items (The union size of A and B)" ),
					new FlaggedOption( "e", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed for each pair of probabilities" ),
					new FlaggedOption( "s", JSAP.DOUBLE_PARSER, "0.1", JSAP.NOT_REQUIRED, 's', "step", "The step for each probability" ),
					new FlaggedOption( "w", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'w', "while", "repeat each estimate w times and return the mean" ),
					new Switch( "r", 'r', "modifiederror", "If true compute a modified version of the relative error: estimate+1/truth+1" ),
					new Switch( "p", 'p', "point", "If true perform a test with given A-probability and B-probability: require to set a and b" ),
					new FlaggedOption( "a", JSAP.DOUBLE_PARSER, "0.1", JSAP.NOT_REQUIRED, 'a', "a", "A-probability" ),
					new FlaggedOption( "b", JSAP.DOUBLE_PARSER, "0.9", JSAP.NOT_REQUIRED, 'b', "b", "B-probability" ),
					
			});
		
		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );
		
		boolean point = jsapResult.getBoolean( "p" );
		
		if( point && !( jsapResult.userSpecified( "a" ) && jsapResult.userSpecified( "b" ) ) ){
			LOGGER.error( "For a single experiment you must set a and b" );
			System.exit( 1 );
		}
		if( point && jsapResult.userSpecified( "s" ) ){
			LOGGER.error( "For a single experiment you must not set s" );
			System.exit( 1 );
		}
		if( !point && ( jsapResult.userSpecified( "a" ) || jsapResult.userSpecified( "b" ) ) ){
			LOGGER.error( "For a multiple experiments you must not set a and b" );
			System.exit( 1 );
		}
		
		n = jsapResult.getInt( "u" );
		k = jsapResult.getInt( "k" );
		item = jsapResult.getInt( "i" );
		experiments = jsapResult.getInt( "e" );
		step = jsapResult.getDouble( "s" );
		repeat = jsapResult.getInt( "w" ); 
		
		jaccardGlobalErrors = new SummaryStats[ STRATEGIES.length ];
		for( int h=0; h<STRATEGIES.length; h++ ) jaccardGlobalErrors[ h ] = new SummaryStats();
		
		if( point ){
			double thA =jsapResult.getDouble( "a" );
			double thAB =jsapResult.getDouble( "b" );
			if( thA > thAB ){
				LOGGER.error( "b has to be greater than a" );
				System.exit( 1 );
			}
			iterateExperiments( thA, thAB );
		} else {
			for( double thA=0.1; thA<1; thA=thA+step )
				for( double thAB=thA; thAB<1; thAB=thAB+step ){
					iterateExperiments( thA, thAB );
				}	
		}
		
		System.out.println( "CASES IN WHICH RELATIVE ERROR is INFINITE: " + infiniteJaccardError );
		Date dt2 = new Date();
		LOGGER.info( dt2.toString() + " Ending Experiment..." );
		System.out.println( dt.toString() + " Ending Experiment..." );
	}
	
	
	private static void iterateExperiments( double thA, double thAB ) {
		double thB=1;
		String param = "A=" + thA + " AB=" +thAB + " B=" + thB + " ";
		LOGGER.info( "**************************************" );
		LOGGER.info( "PARAMETERS: " + param + " " );
		SummaryStats unionErrorStats = new SummaryStats();
		SummaryStats intersectionErrorStats = new SummaryStats();
		for( int ex = 0; ex<experiments; ex++ ){
			LOGGER.info( "RUNNING EXPERIMENT: " + ex );
			new CounterTestAll().test( thA, thAB, thB, unionErrorStats, intersectionErrorStats );
		}
		if(!PRINT_SIZE) System.out.println( param + "UNION " + unionErrorStats.toString() );
		if(!PRINT_SIZE) System.out.println( param + "INTERSECTION " + intersectionErrorStats.toString() );
		for( int cnt=0; cnt<STRATEGIES.length; cnt++ ) System.out.println( "Summary: " + STRATEGIES[cnt] + " " + param + " JACCARD " + jaccardGlobalErrors[cnt] );
	}


	private void test( double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats ) {
		
		CounterArray[][] allCounterArray = new CounterArray[ STRATEGIES.length ][];
		for( int i =0; i<STRATEGIES.length; i++ ){
			if( STRATEGIES[i].equals( "KBottomDensity" ) ) {				
				KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
				for(int j=0; j<counterArray.length; j++){
					KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, "DensityBasedKBottomCounterJaccardEstimateStrategy" );
					counterArray[j] = ca;
				}
				allCounterArray[ i ] = counterArray;
			}
			if( STRATEGIES[i].equals( "KBottomThorup" ) ) {
				KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
				for(int j=0; j<counterArray.length; j++){
					KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, "ThorupKBottomCounterJaccardEstimateStrategy" );
					counterArray[j] = ca;
				}
				allCounterArray[ i ] = counterArray;
			}
			if( STRATEGIES[i].equals( "KBottomIncremental" ) ) {
				KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
				for(int j=0; j<counterArray.length; j++){
					KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, "IncrementalKBottomCounterJaccardEstimateStrategy" );
					counterArray[j] = ca;
				}
				allCounterArray[ i ] = counterArray;
			}
			if(	STRATEGIES[i].equals( "KMins" ) ) {
				JaccardKMins[] counterArray = new JaccardKMins[repeat];
				for(int j=0; j<counterArray.length; j++){
					JaccardKMins ca = new JaccardKMins( k, 2, n );
					counterArray[j] = ca;
				}
				allCounterArray[ i ] = counterArray;
			}
			if( STRATEGIES[i].equals( "KPartition" ) ) {
				KPartitionCounterArray[] counterArray = new KPartitionCounterArray[repeat];
				for(int j=0; j<counterArray.length; j++){
					KPartitionCounterArray ca = new KPartitionCounterArray( k, 2, n );
					counterArray[j] = ca;
				}
				allCounterArray[ i ] = counterArray;
			}
		}
		
		test( thA, thAB, thB, allCounterArray ); 
	}
	
	
	private void test( double thA, double thAB, double thB, CounterArray[][] counterArray ) {
		int aCount = 0;
		int bCount = 0;
		int intersection = 0;
		int a=0;
		int b=1;
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		
		boolean marked[] = new boolean[ n ];
		BooleanArrays.fill( marked, false );
		
		for( int i = 1; i<= item; i++ ){
			double coin = random.nextDouble();
			int element = random.nextInt( n );
			if( marked[element] ) continue;
			marked[element]=true;
			if ( coin < thA ){
				for( int cnt=0; cnt<counterArray.length; cnt++)
					for(int j=0; j<counterArray[cnt].length; j++){
						counterArray[cnt][j].add( a, element );
					}
				aCount++;
			} else if ( coin < thAB ) {
				intersection++;
				for( int cnt=0; cnt<counterArray.length; cnt++)
					for(int j=0; j<counterArray[cnt].length; j++){
						counterArray[cnt][j].add( a, element );
						counterArray[cnt][j].add( b, element );
					}
				aCount++;
				bCount++;
			} else if ( coin < thB ){
				for( int cnt=0; cnt<counterArray.length; cnt++)
					for(int j=0; j<counterArray[cnt].length; j++){
						counterArray[cnt][j].add( b, element );
					}
				bCount++;
			}
			int union = aCount+bCount-intersection;
			double jaccard = union != 0 ? intersection/(double)union : -1;
			
			if( i%item == 0){
				LOGGER.debug( "UNION " + union );
				LOGGER.debug( "INTERSECTION " + intersection );
				if (DEBUG_ENABLED) LOGGER.debug( "A-size: " + aCount );
				if (DEBUG_ENABLED) LOGGER.debug( "B-size: " + bCount );
				
				for( int cnt=0; cnt<counterArray.length; cnt++){
					LOGGER.info( "Estimating with " + STRATEGIES[cnt] );
					SummaryStats jaccardStats = new SummaryStats();
					SummaryStats jaccardErrorStats = new SummaryStats();
					for(int j=0; j<counterArray[cnt].length; j++){
						//double aEstimate = counterArray[cnt][j].count( a );
						//if (DEBUG_ENABLED) LOGGER.debug( "A-size: " + aCount + " A-estimate: " + aEstimate );
						//double bEstimate = counterArray[cnt][j].count( b );
						//if (DEBUG_ENABLED) LOGGER.debug( "B-size: " + bCount + " B-estimate: " + bEstimate );

						if( TEST_JACCARD && counterArray[cnt][j] instanceof JaccardCounterArray ){
							double jj=((JaccardCounterArray)counterArray[cnt][j]).jaccardCoefficient( a, b );
							jaccardStats.add( jj );
							if (PRINT_SIZE) LOGGER.info( "JACCARD: " + intersection/(double)union + " JACCARD-estimate: " + jj );
							if( jaccard == 0 && jj !=0 ) {
								infiniteJaccardError ++;
								LOGGER.error( "While using " + STRATEGIES[cnt] + ", jaccard is 0 but estimated not" );
							}
							else{
								double jaccardError = jaccard == 0 ? 0 : Math.abs(jj-jaccard)/jaccard;
								jaccardErrorStats.add( jaccardError );
								jaccardGlobalErrors[cnt].add(jaccardError);
							}
						}
					}
					LOGGER.info( "STRATEGY: " + STRATEGIES[cnt] + " JACCARD: " + jaccard + " JACCARD-estimate: " + jaccardStats + " JACCARD-error: " + jaccardErrorStats  );
				}
			}		
		}
	}
	
	
}
