package it.unimi.di.triangle.test;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.di.triangle.ApproximateTriangleCounterHyperANF;
import it.unimi.di.triangle.ExactTriangleCounter;
import it.unimi.di.triangle.TriangleCounter;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.Transform;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;


public class ExactVsHyperLogLog {

	private final static Logger LOGGER = LoggerFactory.getLogger( ExactVsHyperLogLog.class );
	
	public static void main( String[] arg ) throws JSAPException, IOException{
		
		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(), "Compare Exact Method and a method based on HyperLogLog counters to compute the number of triangles in an undirected graph",
				new Parameter[] {
					new FlaggedOption( "graph", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'g', "graph", "The graph (it will be symmetrized)" ),
					new FlaggedOption( "log2m", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'l', "log2m", "Registers to be used (for the approximated method)" ),
					new FlaggedOption( "experiments", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'e', "experiments", "The experiments to be performed (for the approximated method)" ),
			});

			JSAPResult jsapResult = jsap.parse( arg );
			if ( jsap.messagePrinted() ) System.exit( 1 );

			final String g = jsapResult.getString( "graph" );
			final int registers = jsapResult.getInt( "log2m" );
			final int experiments = jsapResult.getInt( "experiments" );
			
			ImmutableGraph graph = ImmutableGraph.load( g );
			ImmutableGraph graphSymmetric = Transform.symmetrize( graph );
			
			LOGGER.info( "Symmetrizing graph " + g );
			
			ApproximateTriangleCounterHyperANF approximation = new ApproximateTriangleCounterHyperANF( graphSymmetric, registers );
			double sum = 0;
			for( int i=0; i<experiments; i++ ){
				double rst = approximation.getNumberOfTriangles();
				LOGGER.info( "Experiment " + i + " Estimated Number of Triangles: " + rst + "( avg " + rst/(i+1) + " )" );
				sum += rst;
				approximation.clear();
			}
			double approximatedResult = Math.round( sum/experiments );
			LOGGER.info( "Summary: Estimated Number of Triangles " + approximatedResult );
			
			
			TriangleCounter exact = new ExactTriangleCounter( graphSymmetric );
			double exactResult = exact.getNumberOfTriangles();
			
			LOGGER.info( "Summary: Number of Triangles " + exactResult ); 
	}
	
	
}
