package it.unimi.di.counter.test;

import java.util.Date;

import it.unimi.di.counter.CounterArray;
import it.unimi.di.counter.JaccardCounterArray;
import it.unimi.di.counter.JaccardKMins;
import it.unimi.di.counter.KBottomJaccardCounterArray;
import it.unimi.di.counter.KPartitionCounterArray;
import it.unimi.dsi.fastutil.booleans.BooleanArrays;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.stat.SummaryStats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;

public class CounterTestDeterministicSets {


	private final static Logger LOGGER = LoggerFactory.getLogger( CounterTestDeterministicSets.class );
	private final static boolean DEBUG_ENABLED = true;
	private static final String[] STRATEGIES = { "KBottomDensity", "KBottomThorup", "KMins", "KPartition" };

	public static void main( String[] arg ) throws JSAPException {

		Date dt = new Date();
		LOGGER.info( dt.toString() + " Starting Experiment..." );
		System.out.println( dt.toString() + " Starting Experiment..." );
		SimpleJSAP jsap = new SimpleJSAP( CounterTest.class.getName(), "Test Counters, by assigning elements of a universe to two sets A and B, by varying their probability to get elements",
				new Parameter[] {
			new FlaggedOption( "u", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'u', "universesize", "The size of the universe" ),
			new FlaggedOption( "k", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "The logarithm of the number of registers (log2m for HyperLogLog) or k (Cohen)" ),
			new FlaggedOption( "j", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'j', "jaccard", "The jaccard coefficient" ),
			new FlaggedOption( "e", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed" ),
			new FlaggedOption( "a", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'a', "a", "A size" ),
			new FlaggedOption( "b", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'b', "b", "B size (any b such that b>a is skipped)" )

		} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		

		int n = jsapResult.getInt( "u" );
		String kString = jsapResult.getString( "k" );
		String[] kStringArray = kString.trim().split( "," );
		int[] k = new int[ kStringArray.length ];
		for ( int i = 0; i < k.length; i++ )
			k[ i ] = Integer.parseInt( kStringArray[ i ] );
		int experiments = jsapResult.getInt( "e" );

		String[] aArray = jsapResult.getString( "a" ).trim().split( "," );
		String[] bArray = jsapResult.getString( "b" ).trim().split( "," );
		String[] jArray = jsapResult.getString( "j" ).trim().split( "," );

		CounterTestDeterministicSets test = new CounterTestDeterministicSets();
		int countThread = 0;
		for( int aI = 0; aI<aArray.length; aI++ )
			for( int bI = 0; bI<bArray.length; bI++ ){
				try{
					int a = Integer.parseInt( aArray[aI] );
					int b = Integer.parseInt( bArray[bI] );
					if( b>a ) {
						LOGGER.warn( "Skipping a= " + a + " b=" + b + " since b>a" );
						continue;
					}
					for( int jI = 0; jI<jArray.length; jI++ ){
						double j = Double.parseDouble( jArray[jI] );
						test.new ExperimentRunner( countThread, n, k, a, b, j, experiments ).start();
						countThread++;
					}
				}catch( Exception e ){
					LOGGER.error( "Exception while processing a=" + aArray[aI] + " and b=" +bArray[bI], e );
				}

			}
				
		
	}
	
	
	private class ExperimentRunner extends Thread{
		
		private final int id;
		private final int n;
		private final int[] kArray; 
		private final int a;
		private final int b; 
		private double jaccard;
		private final int experiments;
		private int infiniteJaccardError;
		
		private final String header;
		
		private IntSet cA;
		private IntSet cB;
		
		public ExperimentRunner( int id, int n, int[] kArray, int a, int b, double jaccard, int experiments ){
			this.id= id;
			this.n=n;
			this.kArray=kArray;
			this.a=a;
			this.b=b;
			this.jaccard=jaccard;
			this.experiments=experiments;
			infiniteJaccardError = 0;
			header="[ExperimentRunner"+id+"]-";
			logInfo( "Creating ExperimentRunner with " +toString() );
		}
		
		public String toString(){
			String kString="k=";
			for( int myk : kArray ) kString+=myk+",";
			return "id="+id+", n="+n+", "+ kString +" a="+a+", b="+b+", jaccard="+jaccard+", experiments="+experiments;
		}
		
		private void setSets( int n, int a, int b, double jaccard ) {
			cA = new IntOpenHashSet();
			cB = new IntOpenHashSet();
			int aCount = 0;
			int bCount = 0;
			int inters = (int)( ( jaccard * ( a + b ) ) / ( 1 + jaccard ) );
			if ( DEBUG_ENABLED ) logDebug( "Intersection: " + inters );
			for ( int i = 0; i < inters; i++ ) {
				cA.add( i );
				cB.add( i );
				aCount++;
				bCount++;
			}
			for ( int i = inters; i < a; i++ ) {
				cA.add( i );
				aCount++;
			}
			for ( int i = a; i < b + a - inters; i++ ) {
				cB.add( i );
				bCount++;
			}
			if ( DEBUG_ENABLED ) logDebug( "A-count: " + aCount );
			if ( DEBUG_ENABLED ) logDebug( "B-count: " + bCount );
			
		}

		private void logInfo( String output ){
			LOGGER.info( header+output );
		}

		private void logDebug( String output ){
			LOGGER.debug( header+output );
		}
		
		private void logWarn( String output ){
			LOGGER.warn( header+output );
		}


		public void run() {
			long start = System.currentTimeMillis();
			String param = "A=" + a + " j=" +jaccard + " B=" + b + " ";
			logInfo( "**************************************" );
			logInfo( "PARAMETERS: " + param + " " );

			setSets( n, a, b, jaccard );
			IntSet currentA = cA;
			IntSet currentB = cB;
			
			logInfo( "A-size: " + cA.size() );
			logInfo( "B-size: " + cB.size() );
			
			IntSet unionSet = new IntOpenHashSet( cA );
			unionSet.addAll( cB );
			int union = unionSet.size();
			int intersection = cA.size() + cB.size() - unionSet.size();
			jaccard = (double)intersection/union;
			logInfo( "Jaccard: " + jaccard );
			

			for( int k : kArray ){

				int repeat=1;
				SummaryStats[] jaccardGlobalErrors = new SummaryStats[ STRATEGIES.length ];
				for ( int h = 0; h < STRATEGIES.length; h++ )	jaccardGlobalErrors[ h ] = new SummaryStats();

				for( int exp =0; exp<experiments; exp++ ){

					CounterArray[][] allCounterArray = new CounterArray[ STRATEGIES.length ][];
					for( int i =0; i<STRATEGIES.length; i++ ){
						if( STRATEGIES[i].equals( "KBottomDensity" ) ) {				
							KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, "DensityBasedKBottomCounterJaccardEstimateStrategy" );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						if( STRATEGIES[i].equals( "KBottomThorup" ) ) {
							KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, "ThorupKBottomCounterJaccardEstimateStrategy" );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						if( STRATEGIES[i].equals( "KBottomIncremental" ) ) {
							KBottomJaccardCounterArray[] counterArray = new KBottomJaccardCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								KBottomJaccardCounterArray ca = new KBottomJaccardCounterArray( k, 2, n, "IncrementalKBottomCounterJaccardEstimateStrategy" );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						if(	STRATEGIES[i].equals( "KMins" ) ) {
							JaccardKMins[] counterArray = new JaccardKMins[repeat];
							for(int j=0; j<counterArray.length; j++){
								JaccardKMins ca = new JaccardKMins( k, 2, n );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						if( STRATEGIES[i].equals( "KPartition" ) ) {
							KPartitionCounterArray[] counterArray = new KPartitionCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								KPartitionCounterArray ca = new KPartitionCounterArray( k, 2, n );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
					}


					test( n, currentA, currentB, jaccard, allCounterArray, jaccardGlobalErrors );
				}
				for( int cnt=0; cnt<STRATEGIES.length; cnt++ ) logInfo( "Summary(k=" + k + "): " + STRATEGIES[cnt] + " " + param + " JACCARD " + jaccardGlobalErrors[cnt] );
				for( int cnt=0; cnt<STRATEGIES.length; cnt++ ) System.out.println( "Summary(k=" + k + "): " + STRATEGIES[cnt] + " " + param + " JACCARD " + jaccardGlobalErrors[cnt] );
			}
			logInfo( "CASES IN WHICH RELATIVE ERROR is INFINITE: " + infiniteJaccardError );
			Date dt2 = new Date();
			logInfo( dt2.toString() + " Ending Experiment with parameters: " + param + " in " + (System.currentTimeMillis() - start) + "ms" );
		}



		private void test( int n, IntSet cA, IntSet cB, double jaccard, CounterArray[][] counterArray, SummaryStats[] jaccardGlobalErrors ) {

			boolean marked[] = new boolean[ n ];
			BooleanArrays.fill( marked, false );
			int a = 0;
			int b = 1;
			for ( int element : cA ) {
				for ( int cnt = 0; cnt < counterArray.length; cnt++ )
					for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {
						counterArray[ cnt ][ j ].add( a, element );
					}

			}
			for ( int element : cB ) {
				for ( int cnt = 0; cnt < counterArray.length; cnt++ )
					for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {
						counterArray[ cnt ][ j ].add( b, element );
					}

			}

			for ( int cnt = 0; cnt < counterArray.length; cnt++ ) {
				logInfo( "Estimating with " + STRATEGIES[ cnt ] );
				SummaryStats jaccardStats = new SummaryStats();
				SummaryStats jaccardErrorStats = new SummaryStats();
				for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {
					double jj = ( (JaccardCounterArray)counterArray[ cnt ][ j ] ).jaccardCoefficient( a, b );
					jaccardStats.add( jj );
					logInfo( "JACCARD: " + jaccard + " JACCARD-estimate: " + jj );
					if ( jaccard == 0 && jj != 0 ) {
						infiniteJaccardError++;
						logWarn( "While using " + STRATEGIES[ cnt ] + ", jaccard is 0 but estimated jaccard is not" );
					}
					else {
						double jaccardError = jaccard == 0 ? 0 : Math.abs( jj - jaccard ) / jaccard;
						jaccardErrorStats.add( jaccardError );
						jaccardGlobalErrors[ cnt ].add( jaccardError );
					}

				}
				logInfo( "STRATEGY: " + STRATEGIES[ cnt ] + " JACCARD: " + jaccard + " JACCARD-estimate: " + jaccardStats + " JACCARD-error: " + jaccardErrorStats );
			}
		}
		
		
	}

	

}
