package it.unimi.di.triangle;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleArrays;
import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.LazyIntIterator;
import it.unimi.dsi.webgraph.NodeIterator;

public class ExactTriangleCounter implements TriangleCounter {

	private final static Logger LOGGER = LoggerFactory.getLogger( ExactTriangleCounter.class );
	
	private final ImmutableGraph graph;
	
	public ExactTriangleCounter( ImmutableGraph graph ){
		this.graph =graph;
	}
	
	public void getNumberOfTrianglesForEachNode( String file ) throws FileNotFoundException{
		
		PrintStream out1 = new PrintStream( file );
		double[] rst = getNumberOfTrianglesForEachNode();
		for( int j=0; j<graph.numNodes(); j++ ) out1.println( j + " " + rst[j] );
		out1.close();
	}
	
	public double getNumberOfTriangles(){
		double count= 0;
		
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		int edges = 0;
		NodeIterator iter = graph.nodeIterator();
		while( iter.hasNext() ){
			pl.update();
			int u=iter.nextInt();
			LazyIntIterator successors = graph.successors( u );
			int d = graph.outdegree( u );	
			while( d-- != 0 ) {
				int v=successors.nextInt();
				//for each arc (u,v)
				if( u>=v ) continue;
				edges ++;
				int cont = trianglesInvolvingEdge( u , v );
				if( LOGGER.isTraceEnabled() ) LOGGER.trace( "Edge " + u + "," + v + " triangles exact: " + cont );
				count += cont;
			}
		}
		assert count%3==0 : count;
		count = count/3;
		pl.done();
		LOGGER.info( "Edges analyzed " + edges );
		return count; //we are counting in both directions
	}
	
	public int trianglesInvolvingEdge( int u, int v ){
		int count = 0;
		LazyIntIterator successors2 = graph.successors( v );
		int d2 = graph.outdegree( v );	
		while( d2-- != 0 ) {
			int w=successors2.nextInt();
			//for each w neighboor of v
			if( arcExists( w, u ) ) {
				count++;
			}
		}
		return count;
	}
	
	private boolean arcExists( int u, int v ){
		LazyIntIterator successors = graph.successors( u );
		int d = graph.outdegree( u );	
		while( d-- != 0 ) {
			int w=successors.nextInt();
			if( w==v ) return true;
		}
		return false;
	}


	@Override
	public double[] getNumberOfTrianglesForEachNode() {
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		int edges = 0;
		double[] cc = new double[ graph.numNodes() ];
		DoubleArrays.fill( cc, 0 );
		NodeIterator iter = graph.nodeIterator();
		while( iter.hasNext() ){
			pl.update();
			int u=iter.nextInt();
			LazyIntIterator successors = graph.successors( u );
			int d = graph.outdegree( u );	
			while( d-- != 0 ) {
				int v=successors.nextInt();
				//for each arc (u,v)
				if( u>=v ) continue;
				edges ++;
				int cont = trianglesInvolvingEdge( u , v );
				LOGGER.trace( "Edge " + u + "," + v + " triangles exact: " + cont );
				cc[ u ] += cont;
				cc[ v ] += cont;
			}
		}
		for( int j =0; j< cc.length; j++ ) cc[j]=cc[j]/2;
		pl.done();
		LOGGER.info( "Edges analyzed " + edges );
		return cc;
	}
	
	@Override
	public DoubleArrayList getNumberOfTrianglesForEachEdge() {
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		int edges = 0;
		DoubleArrayList cc = new DoubleArrayList();
		NodeIterator iter = graph.nodeIterator();
		while( iter.hasNext() ){
			pl.update();
			int u=iter.nextInt();
			LazyIntIterator successors = graph.successors( u );
			int d = graph.outdegree( u );	
			while( d-- != 0 ) {
				int v=successors.nextInt();
				//for each arc (u,v)
				if( u>=v ) continue;
				edges ++;
				int cont = trianglesInvolvingEdge( u , v );
				LOGGER.trace( "Edge " + u + "," + v + " triangles exact: " + cont );
				cc.add( cont );
			}
		}
		pl.done();
		LOGGER.info( "Edges analyzed " + edges );
		return cc;
	}
}
