package it.unimi.di.counter.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;

import it.unimi.di.triangle.ApproximateTriangleCounterHyperANF;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.util.HyperLogLogCounterArray;
import it.unimi.dsi.util.XorShift1024StarRandom;

public class HyperLogLogCounterTest {
	
	static int n = 10000000;
	static int log2m = 12;
	static int item = 10000;
	static int experiments = 1000;
	static double step = 0.1;
	static int repeat = (int)(Math.log( n )+1);
	
	
	private final static Logger LOGGER = LoggerFactory.getLogger( ApproximateTriangleCounterHyperANF.class );
	private final static boolean DEBUG_ENABLED = false;
	private final static boolean PRINT_SIZE = true;
	
	private static int infiniteUnionError=0;
	private static int infiniteIntersectionError=0;
	private static boolean modifiederror=true;
	
	public static void main( String[] arg ) throws JSAPException{
		
		SimpleJSAP jsap = new SimpleJSAP( HyperLogLogCounterTest.class.getName(), "Test HyperLogLogCounters, by assigning elements of a universe to two sets A and B, by varying their probability to get elements",
				new Parameter[] {
					new FlaggedOption( "u", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'u', "universesize", "The size of the universe" ),
					new FlaggedOption( "l", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'l', "log2m", "The logarithm of the number of registers" ),
					new FlaggedOption( "i", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'i', "item", "The items (The union size of A and B)" ),
					new FlaggedOption( "e", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed for each pair of probabilities" ),
					new FlaggedOption( "s", JSAP.DOUBLE_PARSER, "0.1", JSAP.NOT_REQUIRED, 's', "step", "The step for each probability" ),
					new FlaggedOption( "w", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'w', "while", "repeat each estimate w times and take the mean" ),
					new Switch( "r", 'r', "modifiederror", "If true compute a modified version of the relative error: estimate+1/truth+1" ),
			});
		
		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );
		
		n = jsapResult.getInt( "u" );
		log2m = jsapResult.getInt( "l" );
		item = jsapResult.getInt( "i" );
		experiments = jsapResult.getInt( "e" );
		step = jsapResult.getDouble( "s" );
		modifiederror = jsapResult.getBoolean( "r" );
		repeat = jsapResult.getInt( "w" ); 
		
		for( double thA=0.1; thA<1; thA=thA+step )
			for( double thAB=thA; thAB<1; thAB=thAB+step ){
				double thB=1;
				String param = " A= " + thA + " AB= " +thAB + " B= " + thB + " ";
				SummaryStats unionErrorStats = new SummaryStats();
				SummaryStats intersectionErrorStats = new SummaryStats();
				for( int ex = 0; ex<experiments; ex++ ) new HyperLogLogCounterTest().testUnionAndIntersection( item, thA, thAB, thB, unionErrorStats, intersectionErrorStats, repeat );
				if(!PRINT_SIZE) System.out.println( param + "UNION " + unionErrorStats.toString() );
				if(!PRINT_SIZE) System.out.println( param + "INTERSECTION " + intersectionErrorStats.toString() );
			}
		System.out.println( "infiniteUnionError: " + infiniteUnionError + " infiniteIntersectionError:" + infiniteIntersectionError );
		//new HyperLogLogCounterTest().testUnionAndIntersection( 10000, 0.3, 0.5, 1);
	}
	
	
	
	public void testUnionAndIntersection( int iterate, double thA, double thAB, double thB, SummaryStats unionErrorStats, SummaryStats intersectionErrorStats, int repeat ) {
		int aCount = 0;
		int bCount = 0;
		int intersection = 0;
		
		HyperLogLogCounterArray[] counterArray = new HyperLogLogCounterArray[repeat];
		for(int j=0; j<counterArray.length; j++){
			HyperLogLogCounterArray ca = new HyperLogLogCounterArray( 2, n, log2m, new XorShift1024StarRandom().nextLong() );
			counterArray[j] = ca;
		}
		
		int a=0;
		int b=1;
		XorShift1024StarRandom random = new XorShift1024StarRandom();

			for( int i = 1; i<= iterate; i++ ){
				//TODO: improve this with a mark vector, the same element could be extracted twice
				double coin = random.nextDouble();
				int element = random.nextInt( n );
				if ( coin < thA ){
					for(int j=0; j<counterArray.length; j++){
						counterArray[j].add( a, element );
					}
					aCount++;
				} else if ( coin < thAB ) {
					intersection++;
					for(int j=0; j<counterArray.length; j++){
						counterArray[j].add( a, element );
						counterArray[j].add( b, element );
					}
					aCount++;
					bCount++;
				} else if ( coin < thB ){
					for(int j=0; j<counterArray.length; j++){
						counterArray[j].add( b, element );
					}
					bCount++;
				}

				if( i%iterate == 0){
					SummaryStats intersectionStats = new SummaryStats();
					for(int j=0; j<counterArray.length; j++){
						long[] aArrCount= new long[ counterArray[j].counterLongwords ];
						counterArray[j].getCounter( a, aArrCount );
						double aEstimate2 = counterArray[j].count( aArrCount, 0 );
						double aEstimate = counterArray[j].count( a );
						if (DEBUG_ENABLED) LOGGER.debug( "A-size: " + aCount + " A-estimate: " + aEstimate );
						if (DEBUG_ENABLED) LOGGER.debug( "A-size: " + aCount + " A-estimate2: " + aEstimate2 );
						long[] bArrCount= new long[ counterArray[j].counterLongwords ];
						counterArray[j].getCounter( b, bArrCount );
						double bEstimate2 = counterArray[j].count( bArrCount, 0 );
						double bEstimate = counterArray[j].count( b );
						if (DEBUG_ENABLED) LOGGER.debug( "B-size: " + bCount + " B-estimate: " + bEstimate );
						if (DEBUG_ENABLED) LOGGER.debug( "B-size: " + bCount + " B-estimate2: " + bEstimate2 );
						counterArray[j].max( aArrCount, bArrCount );
						int union = aCount+bCount-intersection;
						double unionEstimate = counterArray[j].count( aArrCount, 0 );
						double intersectionEstimate = Math.max( 0, aEstimate + bEstimate - unionEstimate );
						intersectionStats.add( intersectionEstimate );
						double unionError = 0;
						double intersectionError = 0;
						if( union==0 ){
							unionError = (unionEstimate!=0 ) ? Double.MAX_VALUE: 0;
						}else unionError = unionEstimate/union;
						if( intersection==0 ){
							intersectionError = (intersectionEstimate!=0 ) ? Double.MAX_VALUE: 0;
						}else intersectionError = intersectionEstimate/intersection;
						if( unionError == Double.MAX_VALUE ) infiniteUnionError++;
						if( intersectionError == Double.MAX_VALUE ) infiniteIntersectionError++;
						if( modifiederror ) {
							unionError = (unionEstimate+1)/(union+1);
							intersectionError = (intersectionEstimate+1)/(intersection+1);
						}
						if (PRINT_SIZE) LOGGER.info( "UNION: " + union + " UNION-estimate: " + unionEstimate + " UNION-error: " + unionError );
						if (PRINT_SIZE) LOGGER.info( "INTERSECTION: " + intersection + " INTERSECTION-estimate: " + intersectionEstimate + " INTERSECTION-error: " + intersectionError  );
						unionErrorStats.add( (unionEstimate+1) / (union+1) );
						intersectionErrorStats.add( (intersectionEstimate+1) / (intersection+1) );
					}

					double meanIntersection = intersectionStats.mean();
					double intersectionError = (meanIntersection+1)/(intersection+1);
					if (PRINT_SIZE) LOGGER.info( "REPEATED INTERSECTION: " + intersection + " INTERSECTION-estimate: " + meanIntersection + " INTERSECTION-error: " + intersectionError  );

					
				}		
		}
	}
	 
}
