package it.unimi.di.counter.test;

import it.unimi.di.counter.LogCounterArray;
import it.unimi.dsi.util.XorShift1024StarRandom;

public class LogCounterArrayTest extends CounterArrayTest {
	
	public static void main( String[] args ){
		new LogCounterArrayTest().testUnionAndIntersection();
	}
	
	
	public void testUnionAndIntersection(){
		int n = 100000;
		int r = (int)(Math.log( n ) + 10);
		int m = 256;
		
		int iterate = 3000;
		double thA = 0.3;
		double thAB = 0.3;
		double thB = 1;
		
		int aCount = 0;
		int bCount = 0;
		int intersection = 0;
		LogCounterArray.setMathPow2( r );
		LogCounterArray[] counterArray = new LogCounterArray[ n ];
		for( int i = 0; i<n ; i++ ){
			counterArray[i] = new LogCounterArray( r, m );
			counterArray[i].init();
			//System.out.println( i + " " + counterArray[i] );
		}
		LogCounterArray a = new LogCounterArray( r, m );
		//a.init();
		LogCounterArray b = new LogCounterArray( r, m );
		//b.init();
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		XorShift1024StarRandom random2 = new XorShift1024StarRandom();
		for( int i = 1; i<= iterate; i++ ){
			double coin = random.nextDouble();
			int element = random2.nextInt( n );
			if ( coin < thA ){
				a.add( counterArray[ element ] );
				aCount++;
			} else if ( coin < thAB ) {
				intersection++;
				a.add( counterArray[ element ] );
				b.add( counterArray[ element ] );
				aCount++;
				bCount++;
			} else if ( coin < thB ){
				b.add( counterArray[ element ] );
				bCount++;
			}
			
			if( i%iterate == 0){
				//System.out.println( "A-size:" + aCount + " B-size: " + bCount + " Union: " + ( aCount+bCount-intersection ) + " Intersection: " + intersection );
				double aEstimate = a.count();
				double bEstimate = b.count();
				double unionEstimate = a.or( b ).count();
				double intersectionEstimate = aEstimate + bEstimate - unionEstimate;
				//System.out.println( "A-estimate:" + aEstimate + " B-estimate: " + bEstimate + " Union-estimate: " + unionEstimate + " Intersection-estimate: " + intersectionEstimate );
				String parameters = "pA: " + thA + " pAB: " + thAB + " pB: " + thB + " ";
				System.out.println( parameters + "A-size: " + aCount + " A-estimate: " + aEstimate );
				System.out.println( parameters + "B-size: " + bCount + " B-estimate: " + bEstimate );
				System.out.println( parameters + "Union: " + ( aCount+bCount-intersection ) + " Union-estimate: " + unionEstimate );
				System.out.println( parameters + "Intersection: " + intersection + " Intersection-estimate: " + intersectionEstimate );
			}
		}
	}
	
	
}
