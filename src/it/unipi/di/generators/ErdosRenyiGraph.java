package it.unipi.di.generators;
import java.util.LinkedList;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;

import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.util.XorShift1024StarRandom;


public class ErdosRenyiGraph extends GraphGenerator {

	public static void main( String[] arg ) throws JSAPException{
		
		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Generate Erdos Renyii -like graph given the number of nodes and the number of connections one node can choose",
				new Parameter[] {
						new FlaggedOption( "n", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'n', "n", "number of nodes" ),
						new FlaggedOption( "d", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'd', "d", "the connection per node to be chosen" )
				} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		final int n = jsapResult.getInt( "n" );
		final int d = jsapResult.getInt( "d" );
		
		System.out.println( n );
		int[][] ttt = new ErdosRenyiGraph( n, d, new XorShift1024StarRandom().nextLong() ).getGraph();
		for( int[] a : ttt ){
			System.out.println( a[0] + " " + a[1] );
		}
	}
	
	int d;
	
	public ErdosRenyiGraph( int n, int d, long seed ){
		this.nodes = n;
		this.d = d;
		XorShift1024StarRandom random = new XorShift1024StarRandom( seed );
		LinkedList<int[]> ll = new LinkedList<int[]>();
		for( int i=0; i<nodes; i++ ){
			IntArrayList neigh = new IntArrayList();
			for( int j=0; j<d; j++ ){
				int cc = random.nextInt( nodes );
				if( !neigh.contains( cc )){
					neigh.add( cc );
					ll.add( new int[]{ i, cc } );
				}
			}
		}
		result = new int[ ll.size() ][];
		ll.toArray( result );
	}
	
	
}
