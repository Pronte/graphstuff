package it.unimi.di.triangle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import it.unimi.di.counter.DensityKBottomCounterArray;
import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.fastutil.doubles.DoubleArrays;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.Transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;

public class ApproximateTriangleCounterBottomK extends ApproximateTriangleCounter {

	private final static Logger LOGGER = LoggerFactory.getLogger( ApproximateTriangleCounterBottomK.class );
	
	double[] ccClassicalKBottom;
	double[] ccDensityKBottom;
	
	public ApproximateTriangleCounterBottomK( ImmutableGraph graph, int k, int seed, boolean collisions ){
		super( graph, k, seed, collisions );
		setup();
	}
	
	private void setup() {
		sketches = new DensityKBottomCounterArray( k, graph.numNodes(), graph.numNodes(), collisions, seed );
		//sketches = new KBottomJaccardCounterArray( k, graph.numNodes(), graph.numNodes() );
		fillSketches();
		sketches.prepare();
		ccClassicalKBottom = new double[ graph.numNodes() ];
		ccDensityKBottom = new double[ graph.numNodes() ];
		DoubleArrays.fill( ccClassicalKBottom, .0 );
		DoubleArrays.fill( ccDensityKBottom, .0 );
	}
	
	public void processArc( int v, int w, int degreev, int degreew ){
		int vsize = ((DensityKBottomCounterArray)sketches).get( v ).size();
		int wsize = ((DensityKBottomCounterArray)sketches).get( w ).size();
		int inters = (int)((DensityKBottomCounterArray)sketches).intersection( v, w );
		double contributeClassicalKBottom;
		double contributeDensityKBottom;
		
		if( vsize < k && wsize<k ) {
			if( vsize!=degreev ) LOGGER.error( "sketch of " +v + " has size less than k: its degree is " + degreev + " but its sketch has size " + vsize );
			if( wsize!=degreew ) LOGGER.error( "sketch of " +w + " has size less than k: its degree is " + degreew + " but its sketch has size " + wsize );
			contributeClassicalKBottom = inters/2.0;
			contributeDensityKBottom = inters/2.0;
		} else{
			double jClassical = ((DensityKBottomCounterArray)sketches).jaccardCoefficientClassicalWrapper( v, w );
			contributeClassicalKBottom = ( degreev + degreew )* jClassical /( jClassical + 1 )/2.0;
			double jDensity = sketches.jaccardCoefficient( v, w );
			contributeDensityKBottom = ( degreev + degreew )* jDensity /( jDensity + 1 )/2.0;
		}
		
		//else LOGGER.debug( "Triangles involving edge " + w + "," + v + " estimate: " + contribute );
		ccClassicalKBottom[ w ] += (double)contributeClassicalKBottom;
		ccClassicalKBottom[ v ] += (double)contributeClassicalKBottom;
		ccDensityKBottom[ w ] += (double)contributeDensityKBottom;
		ccDensityKBottom[ v ] += (double)contributeDensityKBottom;
		
		if ( LOGGER.isTraceEnabled() ){
			check( v, w, degreev, degreew, contributeClassicalKBottom );
			check( v, w, degreev, degreew, contributeDensityKBottom );
		}
	}
	
	/*
	public void setNumberOfTrianglesForEachNodeWithClassicalAndDensityKBottom() {		
		LOGGER.info( "Starting..." );
		long start = System.currentTimeMillis();
		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";
		ExactTriangleCounter exactCounter = new ExactTriangleCounter( graph );
		NodeIterator iter = graph.nodeIterator();
		
		double[] ccExact = null;
		if ( LOGGER.isTraceEnabled() ) {
			ccExact = new double[ graph.numNodes() ];
			DoubleArrays.fill( ccExact, .0 );
		}

		int edges = 0;
		while ( iter.hasNext() ) {
			pl.update();
			int w = iter.nextInt();
			int dw = iter.outdegree();
			int degreew = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				int degreev = graph.outdegree( v );
				// for each arc (w,v)
				if ( w >= v ) continue;
				edges++;
				
				int vsize = ((DensityKBottomCounterArray)sketches).get( v ).size();
				int wsize = ((DensityKBottomCounterArray)sketches).get( w ).size();
				int inters = (int)sketches.intersection( v, w );
				double contributeClassicalKBottom;
				double contributeDensityKBottom;
				if( vsize < k && wsize<k ) {
					contributeClassicalKBottom = inters;
					contributeDensityKBottom = inters;
				} else{
					double jClassical = sketches.jaccardCoefficientClassicalWrapper( v, w );
					contributeClassicalKBottom = ( degreev + degreew )* jClassical /( jClassical + 1 )/2.0;
					double jDensity = sketches.jaccardCoefficient( v, w );
					contributeDensityKBottom = ( degreev + degreew )* jDensity /( jDensity + 1 )/2.0;
				}
				
				if ( LOGGER.isTraceEnabled() ) {
					int exact = exactCounter.trianglesInvolvingEdge( w, v );
					ccExact[ w ] += exact;
					ccExact[ v ] += exact;
					LOGGER.trace( "Triangles involving edge " + w + "," + v + " estimate: " + contributeClassicalKBottom + " exact: " + exact  + " degree-w: " + degreew + " degree-v: " + degreev );
					if( exact == 0 && contributeClassicalKBottom!=0 ) LOGGER.error( "Exact is 0 while the contribute of this edge is " + contributeClassicalKBottom + ": "
							+ "the sketches are " + sketches.get( v ) + " and " + sketches.get( w ) + "; the adj are " + ToString.IntArrayToString( graph.successorArray( v ) ) + " and " + ToString.IntArrayToString( graph.successorArray( w ) ) );
				}
				//else LOGGER.debug( "Triangles involving edge " + w + "," + v + " estimate: " + contribute );
				ccClassicalKBottom[ w ] += contributeClassicalKBottom;
				ccClassicalKBottom[ v ] += contributeClassicalKBottom;
				ccDensityKBottom[ w ] += contributeDensityKBottom;
				ccDensityKBottom[ v ] += contributeDensityKBottom;
			}
		}
		
		LOGGER.info( "Edges analyzed " + edges );
		LOGGER.info( "Ending in " + ( System.currentTimeMillis() - start ) + "ms." );
	}*/
	
	public static void main( String[] arg ) throws JSAPException, IOException {

		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Compare Exact Method and a method based on K Bottom counters to compute the number of triangles in an undirected graph",
				new Parameter[] {
						new FlaggedOption( "graph", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'g', "graph", "The graph" ),
						new FlaggedOption( "k", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "sketch size to be used" ),
						new FlaggedOption( "experiments", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed" ),
						new FlaggedOption( "output1", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'c', "output1", "The file name for the output of classical Bottom-k" ),
						new FlaggedOption( "output2", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'd', "output2", "The file name for the output of density Bottom-k" ),
						new Switch( "symmetrize", 'y', "symmetrize", "Symmetrize" ),
						new Switch( "exact", 'x', "exact", "Exact" ),
						new Switch( "collision", 'l', "collision", "Allow collisions" ),
						new FlaggedOption( "seed", JSAP.INTEGER_PARSER, Integer.toString(DEFAULT_SEED), JSAP.NOT_REQUIRED, 's', "seed", "seed" )
				} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		final String g = jsapResult.getString( "graph" );
		final int k = jsapResult.getInt( "k" );
		final int experiments = jsapResult.getInt( "experiments" );
		final String file1 = jsapResult.getString( "output1" );
		final String file2 = jsapResult.getString( "output2" );
		int seed = jsapResult.getInt("seed");
		boolean collisions = jsapResult.getBoolean( "collision" );
		
		ImmutableGraph graph = ImmutableGraph.load( g );
		ImmutableGraph graphSymmetric = null;
		if ( jsapResult.getBoolean( "symmetrize" ) ) {
			graphSymmetric = Transform.symmetrize( graph );
			LOGGER.info( "Symmetrizing graph " + g );
		}
		else {
			graphSymmetric = graph;
		}

		if ( jsapResult.getBoolean( "exact" ) ) runForEachNodeTogheterWithExact( k, experiments, graphSymmetric, file1, file2, collisions, seed );
		else runForEachNode( k, experiments, graphSymmetric, file1, file2, collisions, seed );
	}

	public static void runForEachNodeTogheterWithExact( final int k, final int experiments, ImmutableGraph graph, String fileName1, String fileName2, boolean collisions, int seed ) throws FileNotFoundException {
		SummaryStats[] ssClassicalBottomK = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] scClassicalBottomK = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] ssDensityBottomK = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] scDensityBottomK = new SummaryStats[ graph.numNodes() ];
		for( int j=0; j<graph.numNodes(); j++ ) {
			ssClassicalBottomK[j] = new SummaryStats();
			scClassicalBottomK[j] = new SummaryStats();
			ssDensityBottomK[j] = new SummaryStats();
			scDensityBottomK[j] = new SummaryStats();
		}
		double[] ccExact = new ExactTriangleCounter( graph ).getNumberOfTrianglesForEachNode();
		for ( int i = 0; i < experiments; i++ ) {
			ApproximateTriangleCounterBottomK approximation = new ApproximateTriangleCounterBottomK( graph, k, seed, collisions );
			approximation.setNumberOfTrianglesForEachNode();
			for( int j=0; j<graph.numNodes(); j++ ){
				if( ccExact[j] != 0 ) {
					ssClassicalBottomK[j].add( Math.abs( approximation.ccClassicalKBottom[j]-ccExact[j] )/ccExact[j] );			
					ssDensityBottomK[j].add( Math.abs( approximation.ccClassicalKBottom[j]-ccExact[j] )/ccExact[j] );
				}
				else if ( ccExact[j] == 0 && ( approximation.ccClassicalKBottom[j] != 0  || approximation.ccDensityKBottom[j] != 0 ) ) {
					if( ccExact[j] == 0 && approximation.ccClassicalKBottom[j] != 0 ) LOGGER.error( "Experiment " + i + " wrong estimation for node " + j  + ": more than 0 triangles " );
					if( ccExact[j] == 0 && approximation.ccDensityKBottom[j] != 0 ) LOGGER.error( "Experiment " + i + " wrong estimation for node " + j  + ": more than 0 triangles " );
				}
				else {
					ssClassicalBottomK[j].add( Math.abs( approximation.ccClassicalKBottom[j]-ccExact[j] )/(double)ccExact[j] );
					ssDensityBottomK[j].add( Math.abs( approximation.ccDensityKBottom[j]-ccExact[j] )/(double)ccExact[j] );
					scClassicalBottomK[j].add( approximation.ccClassicalKBottom[j] );
					scDensityBottomK[j].add( approximation.ccDensityKBottom[j] );
				}
				
			}
		}
		
		for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "ClassicalBottomK-Summary-error: " + j + " " + ssClassicalBottomK[j].toString() );
		for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "DensityBottomK-Summary-error: " + j + " " + ssClassicalBottomK[j].toString() );
		for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "ClassicalBottomK-Summary-counters: " + j + " " + scClassicalBottomK[j].toString() + " " + ccExact[ j ] );
		for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "DensityBottomK-Summary-counters: " + j + " " + scDensityBottomK[j].toString() + " " + ccExact[ j ] );

		PrintStream out1 = new PrintStream( fileName1 );
		PrintStream out2 = new PrintStream( fileName2 );
		
		for( int j=0; j<graph.numNodes(); j++ ) out1.println( j + " " + scClassicalBottomK[j].mean() + " " + ccExact[ j ] );
		for( int j=0; j<graph.numNodes(); j++ ) out2.println( j + " " + scDensityBottomK[j].mean() + " " + ccExact[ j ] );
		out1.close();
		out2.close();
	}
	
	public static void runForEachNode( final int k, final int experiments, ImmutableGraph graph, String fileName1, String fileName2, boolean collisions, int seed ) throws FileNotFoundException {
		SummaryStats[] ssClassicalBottomK = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] scClassicalBottomK = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] ssDensityBottomK = new SummaryStats[ graph.numNodes() ];
		SummaryStats[] scDensityBottomK = new SummaryStats[ graph.numNodes() ];
		for( int j=0; j<graph.numNodes(); j++ ) {
			ssClassicalBottomK[j] = new SummaryStats();
			scClassicalBottomK[j] = new SummaryStats();
			ssDensityBottomK[j] = new SummaryStats();
			scDensityBottomK[j] = new SummaryStats();
		}
		for ( int i = 0; i < experiments; i++ ) {
			ApproximateTriangleCounterBottomK approximation = new ApproximateTriangleCounterBottomK( graph, k, seed, collisions );
			approximation.setNumberOfTrianglesForEachNode();
			for( int j=0; j<graph.numNodes(); j++ ){
				scClassicalBottomK[j].add( approximation.ccClassicalKBottom[j] );
				scDensityBottomK[j].add( approximation.ccDensityKBottom[j] );
			}
		}
		if( experiments!=1 ){
			for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "ClassicalBottomK-Summary-counters: " + j + " " + scClassicalBottomK[j].toString() );
			for( int j=0; j<graph.numNodes(); j++ ) LOGGER.info( "DensityBottomK-Summary-counters: " + j + " " + scDensityBottomK[j].toString() );			
		}

		
		PrintStream out1 = new PrintStream( fileName1 );
		PrintStream out2 = new PrintStream( fileName2 );
		
		for( int j=0; j<graph.numNodes(); j++ ) out1.println( j + " " + scClassicalBottomK[j].mean() );
		for( int j=0; j<graph.numNodes(); j++ ) out2.println( j + " " + scDensityBottomK[j].mean() );
		out1.close();
		out2.close();
	}
	
}
