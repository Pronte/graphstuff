package it.unimi.di.triangle;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;

import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.Util;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleArrays;
import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.util.HyperLogLogCounterArray;
import it.unimi.dsi.util.XorShift1024StarRandom;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.LazyIntIterator;
import it.unimi.dsi.webgraph.NodeIterator;
import it.unimi.dsi.webgraph.Transform;

public class ApproximateTriangleCounterHyperANF extends HyperLogLogCounterArray implements TriangleCounter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger LOGGER = LoggerFactory.getLogger( ApproximateTriangleCounterHyperANF.class );

	private final static boolean DEBUG_ENABLED = true;

	private final ImmutableGraph graph;

	// private final HyperLogLogCounterArray counters;

	private final static int ensureEnoughRegisters( final int log2m ) {
		if ( log2m < 4 ) throw new IllegalArgumentException( "There must be at least 16 registers per counter" );
		if ( log2m > 60 ) throw new IllegalArgumentException( "There can be at most 2^60 registers per counter" );
		return log2m;
	}

	public ApproximateTriangleCounterHyperANF( ImmutableGraph graph, int log2m ) throws IOException {
		// counters = new HyperLogLogCounterArray( graph.numNodes(), graph.numNodes(),
		// ensureEnoughRegisters( log2m ), new XorShift1024StarRandom().nextLong() );
		super( graph.numNodes(), graph.numNodes(), ensureEnoughRegisters( log2m ), new XorShift1024StarRandom().nextLong() );
		this.graph = graph;
	}

	@Override
	public double getNumberOfTriangles() {
		LOGGER.info( "Starting..." );
		long start = System.currentTimeMillis();

		ProgressLogger pl = new ProgressLogger();
		pl.expectedUpdates = graph.numNodes();
		pl.itemsName = "nodes";

		ExactTriangleCounter exactCounter = new ExactTriangleCounter( graph );

		NodeIterator iter = graph.nodeIterator();
		double[] cc = new double[ graph.numNodes() ];// this is to maintain the clustering
														// coefficient
		double[] ccExact;
		if ( DEBUG_ENABLED ) {
			ccExact = new double[ graph.numNodes() ];
			DoubleArrays.fill( ccExact, .0 );
		}
		DoubleArrays.fill( cc, .0 );

		setup();
		estimatedDegree();

		int edges = 0;
		int exactTotal = 0;
		while ( iter.hasNext() ) {
			pl.update();

			int w = iter.nextInt();
			int dw = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				// int dv = graph.outdegree( v );
				// for each arc (w,v)
				if ( w >= v ) continue;

				edges++;
				long[] wCount = new long[ counterLongwords ];
				getCounter( w, wCount );
				double wc = count( wCount, 0 );

				long[] vCount = new long[ counterLongwords ];
				getCounter( v, vCount );
				double vc = count( vCount, 0 );

				max( wCount, vCount );
				double union = count( wCount, 0 );

				// TODO: decide which one of the following two
				double contribute = Math.max( 0, wc + vc - union );
				// double contribute = Math.max( 0, dw + dv - union );
				LOGGER.debug( "Edge " + w + "," + v + " union estimated: " + union );
				if ( DEBUG_ENABLED ) {
					int exact = exactCounter.trianglesInvolvingEdge( w, v );
					exactTotal += exact;
					ccExact[ w ] += exact;
					ccExact[ v ] += exact;
					LOGGER.debug( "Edge " + w + "," + v + " triangles estimated: " + contribute + " triangles exact: " + exact );
				}
				else LOGGER.debug( "Edge " + w + "," + v + " triangles estimated: " + contribute );
				cc[ w ] += contribute;
				cc[ v ] += contribute;
			}
		}
		pl.done();
		double sum = 0;
		for ( int i = 0; i < cc.length; i++ ) {
			if ( DEBUG_ENABLED ) LOGGER.debug( "Triangle involving " + i + " estimate: " + cc[ i ] + " exact: " + ccExact[ i ] );
			sum += cc[ i ];
		}
		LOGGER.info( "Edges analyzed " + edges );
		LOGGER.info( "Ending in " + ( System.currentTimeMillis() - start ) + "ms." );
		sum = sum / 2; // this is because we add contributes for both the extremes of an edge
		if ( DEBUG_ENABLED ) LOGGER.debug( "Number of Triangles estimated " + sum + " Exact Number of Triangles is " + exactTotal );
		return sum;
	}

	private void setup() {
		NodeIterator iter = graph.nodeIterator();
		while ( iter.hasNext() ) {
			int w = iter.nextInt();
			int dw = iter.outdegree();
			LazyIntIterator successors = iter.successors();
			while ( dw-- != 0 ) {
				int v = successors.nextInt();
				add( w, v );
			}
		}
	}

	private void estimatedDegree() {
		NodeIterator iter = graph.nodeIterator();
		long sum = 0;
		long sumD = 0;
		while ( iter.hasNext() ) {
			int w = iter.nextInt();
			int dw = graph.outdegree( w );
			sumD += dw;
			long[] wCount = new long[ counterLongwords ];
			getCounter( w, wCount );
			double estimate = count( wCount, 0 );
			sum += estimate;
			LOGGER.debug( "Node " + w + " degree estimated: " + ( estimate - 1 ) + " degree exact: " + dw );
		}
		LOGGER.debug( "Number of edges estimated: " + sum + " Number of edges exact: " + sumD );
	}

	public void clear() {
		super.clear();
		super.seed = Util.randomSeed();
	}


	public static void main( String[] arg ) throws JSAPException, IOException {

		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Compare Exact Method and a method based on HyperLogLog counters to compute the number of triangles in an undirected graph",
				new Parameter[] {
						new FlaggedOption( "graph", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'g', "graph", "The graph (it will be symmetrized)" ),
						new FlaggedOption( "log2m", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'l', "log2m", "Registers to be used (for the approximated method)" ),
						new FlaggedOption( "experiments", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'e', "experiments", "The experiments to be performed (for the approximated method)" ),
						new Switch( "symmetrize", 's', "symmetrize", "Symmetrize" ),
				} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		final String g = jsapResult.getString( "graph" );
		final int registers = jsapResult.getInt( "log2m" );
		final int experiments = jsapResult.getInt( "experiments" );

		ImmutableGraph graph = ImmutableGraph.load( g );
		ImmutableGraph graphSymmetric = null;
		if ( jsapResult.getBoolean( "symmetrize" ) ) {
			graphSymmetric = Transform.symmetrize( graph );
			LOGGER.info( "Symmetrizing graph " + g );
		}
		else {
			graphSymmetric = graph;
		}

		ApproximateTriangleCounterHyperANF approximation = new ApproximateTriangleCounterHyperANF( graphSymmetric, registers );
		SummaryStats ss = new SummaryStats();
		for ( int i = 0; i < experiments; i++ ) {
			double rst = approximation.getNumberOfTriangles();
			LOGGER.info( "Experiment " + i + " Estimated Number of Triangles: " + rst );
			ss.add( rst );
			approximation.clear();
		}
		double approximatedResult = ss.mean();
		LOGGER.info( "Summary: Stats Number of Triangles " + ss );
		LOGGER.info( "Summary: Estimated Number of Triangles " + approximatedResult );

	}
	
	@Override
	public double[] getNumberOfTrianglesForEachNode() {
		 throw new RuntimeException( "The method getNumberOfTrianglesForEachNode is not implemented" );
	}

	@Override
	public DoubleArrayList getNumberOfTrianglesForEachEdge() {
		 throw new RuntimeException( "The method getNumberOfTrianglesForEachEdge is not implemented" );
	}
}
