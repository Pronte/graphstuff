package it.unipi.di.generators;

import it.unimi.dsi.webgraph.ArrayListMutableGraph;
import it.unimi.dsi.webgraph.BVGraph;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.Transform;

import java.io.IOException;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StarGraph extends GraphGenerator {

	public StarGraph( int du, int dv, int triangles ){

		LOGGER.debug( "Generating stars with degrees " + du + " and " + dv + " with " + triangles + " common neigh "  );
		LinkedList<int[]> ll = new LinkedList<int[]>();
		ll.add( new int[]{ 0, 1} );
		nodes = 2;
		for( int i=1; i<du-triangles; i++ ){
			ll.add( new int[]{ 0, nodes } );
			nodes ++;
		}
		for( int i=1; i<dv-triangles; i++ ){
			ll.add( new int[]{ 1, nodes } );
			nodes ++;
		}
		for( int i=0; i<triangles; i++ ){
			ll.add( new int[]{ 0, nodes } );
			ll.add( new int[]{ 1, nodes } );
			nodes++;
		}
		result = new int[ ll.size() ][];
		ll.toArray( result );

		if( LOGGER.isTraceEnabled() ) for( int i=0; i<result.length; i++ ) System.out.println( result[i][0] + " " + result[i][1] );
	}


	private final static Logger LOGGER = LoggerFactory.getLogger( GraphGenerator.class );

	private final static int[][] DEGREEs = { {32, 32}, {32, 256}, {32, 1024}, {256, 256}, {256, 1024}, {1024, 1024} };
	private final static int STEP_COMMON_NEIGHs = 8;

	public static void main( String[] args ) throws IOException{
		for( int[] p : DEGREEs ){
			int i = p[0];
			int j = p[1];
			for( int tr=0; tr<Math.min( i, j ); tr+=Math.min( i, j )/STEP_COMMON_NEIGHs ){
				StarGraph ttt= new StarGraph( i, j, tr );
				ImmutableGraph graph = new ArrayListMutableGraph( ttt.getNumberOfNodes(), ttt.getGraph() ).immutableView();		
				ImmutableGraph graph2 = Transform.symmetrize( graph );
				BVGraph.store( BVGraph.class, graph2, "star-"+i+"-"+j+"-"+tr, null );
			}
		}
	}

}
