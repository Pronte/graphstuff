package it.unimi.di.counter.test;

import java.util.Date;

import it.unimi.di.counter.DensityKBottomCounterArray;
import it.unimi.di.counter.JaccardCounterArray;
import it.unimi.di.counter.KBottomCounterArray;
import it.unimi.di.counter.KMins;
import it.unimi.di.counter.KPartitionCounterArray;
import it.unimi.dsi.fastutil.booleans.BooleanArrays;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.stat.SummaryStats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;

public class FastCounterTestDeterministicSets {


	private final static Logger LOGGER = LoggerFactory.getLogger( FastCounterTestDeterministicSets.class );
	private final static boolean DEBUG_ENABLED = true;
	private static final String DEFAULT_STRATEGIES_STRING = "KBottomDensity,KBottomThorup,KMins,KPartition";
	private static String[] CURRENT_STRATEGIES;
	
	public static void main( String[] arg ) throws JSAPException {

		Date dt = new Date();
		LOGGER.info( dt.toString() + " Starting Experiment..." );
		System.out.println( dt.toString() + " Starting Experiment..." );
		SimpleJSAP jsap = new SimpleJSAP( FastCounterTestDeterministicSets.class.getName(), "Test Probabilistic Counters to estimate jaccard coefficient, by assigning elements of a universe to two sets A and B whose size and jaccard coefficient is given. For the parameter whose value is set more than once, all the combinations will be performed",
				new Parameter[] {
			new FlaggedOption( "m", JSAP.STRING_PARSER, DEFAULT_STRATEGIES_STRING, JSAP.NOT_REQUIRED, 'm', "methods", "List of methods separated by ,. Available methods are: KBottomDensity,KBottomThorup,KMins,KPartition. By default all the methods are performed" ),
			new FlaggedOption( "u", JSAP.INTEGER_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'u', "universesize", "The size of the universe" ),
			new FlaggedOption( "k", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'k', "k", "The size for the sketches (more k can be specified separated by ,)" ),
			new FlaggedOption( "j", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'j', "jaccard", "The jaccard coefficient (more jaccard can be specified separated by ,)" ),
			new FlaggedOption( "e", JSAP.INTEGER_PARSER, "1", JSAP.NOT_REQUIRED, 'e', "experiments", "The experiments to be performed" ),
			new FlaggedOption( "a", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'a', "a", "A size (more sizes can be specified separated by ,)" ),
			new FlaggedOption( "b", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, 'b', "b", "B size (more sizes can be specified separated by ,): any b such that b>a is skipped" )
		} );

		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );
		
		CURRENT_STRATEGIES = jsapResult.getString( "m" ).trim().split( "," );
		String[] defaultStrategies = DEFAULT_STRATEGIES_STRING.trim().split( "," );
		for( int i =0; i<CURRENT_STRATEGIES.length; i++ ){
			boolean found=false;
			for( int j =0; j<defaultStrategies.length; j++ ){
				if( CURRENT_STRATEGIES[i].equals( defaultStrategies[j] ) ) {
					found = true;
					break;
				}
			}
			if( !found ) {
				LOGGER.error( CURRENT_STRATEGIES[i] + " is not available" );
				System.err.println( CURRENT_STRATEGIES[i] + " is not available" );
				System.exit( 0 );
			}
		}
		
		int n = jsapResult.getInt( "u" );
		String kString = jsapResult.getString( "k" );
		String[] kStringArray = kString.trim().split( "," );
		int[] k = new int[ kStringArray.length ];
		for ( int i = 0; i < k.length; i++ )
			k[ i ] = Integer.parseInt( kStringArray[ i ] );
		int experiments = jsapResult.getInt( "e" );

		String[] aArray = jsapResult.getString( "a" ).trim().split( "," );
		String[] bArray = jsapResult.getString( "b" ).trim().split( "," );
		String[] jArray = jsapResult.getString( "j" ).trim().split( "," );

		FastCounterTestDeterministicSets test = new FastCounterTestDeterministicSets();
		int countThread = 0;
		for( int aI = 0; aI<aArray.length; aI++ )
			for( int bI = 0; bI<bArray.length; bI++ ){
				try{
					int a = Integer.parseInt( aArray[aI] );
					int b = Integer.parseInt( bArray[bI] );
					if( b>a ) {
						LOGGER.warn( "Skipping a= " + a + " b=" + b + " since b>a" );
						continue;
					}
					for( int jI = 0; jI<jArray.length; jI++ ){
						double j = Double.parseDouble( jArray[jI] );
						
						if( !isDoable( a, b, j ) ) {
							LOGGER.warn( "Skipping experiments with a:" + a + " b:" + b + " j:" + j + " since feasible instance does not exists"  );
							continue;
						}
						
						test.new ExperimentRunner( countThread, n, k, a, b, j, experiments ).start();
						countThread++;
					}
				}catch( Exception e ){
					LOGGER.error( "Exception while processing a=" + aArray[aI] + " and b=" +bArray[bI], e );
				}

			}
				
		
	}
	
	
	private static boolean isDoable( int a, int b, double j ) {
		if( j>b/(double)a  ) return false;
		double inters = (int)( ( j * ( a + b ) ) / ( 1 + j ) );
		if( inters > a || inters > b) return false;
		return true;
	}


	private class ExperimentRunner extends Thread{
		
		private final int id;
		private final int n;
		private final int[] kArray; 
		private final int a;
		private final int b; 
		private double jaccard;
		private final int experiments;
		private int infiniteJaccardError;
		
		private final String header;
		
		private IntSet cA;
		private IntSet cB;
		
		public ExperimentRunner( int id, int n, int[] kArray, int a, int b, double jaccard, int experiments ){
			this.id= id;
			this.n=n;
			this.kArray=kArray;
			this.a=a;
			this.b=b;
			this.jaccard=jaccard;
			this.experiments=experiments;
			infiniteJaccardError = 0;
			header="[ExperimentRunner"+id+"]-";
			logInfo( "Creating ExperimentRunner with " +toString() );
		}
		
		public String toString(){
			String kString="k=";
			for( int myk : kArray ) kString+=myk+",";
			return "id="+id+", n="+n+", "+ kString +" a="+a+", b="+b+", jaccard="+jaccard+", experiments="+experiments;
		}
		
		private void setSets( int n, int a, int b, double jaccard ) {
			
			if( !isDoable( a, b, jaccard )) {
				LOGGER.error( "Experiment with a:" + a + " b:" + b + " j:" + jaccard + " is not doable" );
				throw new IllegalStateException();
			}
			
			cA = new IntOpenHashSet();
			cB = new IntOpenHashSet();
			int aCount = 0;
			int bCount = 0;
			int inters = (int)( ( jaccard * ( a + b ) ) / ( 1 + jaccard ) );
			if ( DEBUG_ENABLED ) logDebug( "Intersection: " + inters );
			for ( int i = 0; i < inters; i++ ) {
				cA.add( i );
				cB.add( i );
				aCount++;
				bCount++;
			}
			for ( int i = inters; i < a; i++ ) {
				cA.add( i );
				aCount++;
			}
			for ( int i = a; i < b + a - inters; i++ ) {
				cB.add( i );
				bCount++;
			}
			if ( DEBUG_ENABLED ) logDebug( "A-count: " + aCount );
			if ( DEBUG_ENABLED ) logDebug( "B-count: " + bCount );
			
			if ( cA.size() != a || cB.size() != b ) {
				LOGGER.error( "Wrong set generation when a:" + a + " b:" + b + " j:" + jaccard );
				throw new IllegalStateException();
			}
			
		}

		private void logInfo( String output ){
			LOGGER.info( header+output );
		}

		private void logDebug( String output ){
			LOGGER.debug( header+output );
		}
		
		private void logWarn( String output ){
			LOGGER.warn( header+output );
		}


		public void run() {
			long start = System.currentTimeMillis();
			String param = "A=" + a + " j=" +jaccard + " B=" + b + " ";
			logInfo( "Running... " );
			logInfo( "PARAMETERS: " + param + " " );
			
			setSets( n, a, b, jaccard );
			IntSet currentA = cA;
			IntSet currentB = cB;
						
			/*
			IntSet copyA = new IntOpenHashSet();
			IntSet copyB = new IntOpenHashSet();
			for( int mmm : currentA ) copyA.add( mmm );
			for( int mmm : currentB ) copyB.add( mmm );
			copyA.addAll( copyB );
			int union = copyA.size();
			*/
			
			IntSet unionSet = new IntOpenHashSet( cA );
			unionSet.addAll( cB );
			int union = unionSet.size();
			
			int intersection = cA.size() + cB.size() - union;
			jaccard = (double)intersection/union;

			logInfo( "Sets created... A-size=" + cA.size() + " B-size=" + cB.size() + " Jaccard=" + jaccard );
			
			if ( cA.size() != a || cB.size() != b ) {
				LOGGER.error( "Wrong set generation when a:" + a + " b:" + b + " j:" + jaccard );
				throw new IllegalStateException();
			}
			
			for( int k : kArray ){
				logInfo( "Running with k="+k+"..." );
				int repeat=1;
				SummaryStats[] jaccardMRAE = new SummaryStats[ CURRENT_STRATEGIES.length ];
				SummaryStats[] jaccardMean = new SummaryStats[ CURRENT_STRATEGIES.length ];
				SummaryStats[] jaccardMRE = new SummaryStats[ CURRENT_STRATEGIES.length ];
				SummaryStats[] jaccardCVRMSE = new SummaryStats[ CURRENT_STRATEGIES.length ];
				
				for ( int h = 0; h < CURRENT_STRATEGIES.length; h++ )	{
					jaccardMRAE[ h ] = new SummaryStats();
					jaccardMRE[ h ] = new SummaryStats();
					jaccardMean[ h ] = new SummaryStats();
					jaccardCVRMSE[ h ] = new SummaryStats();
				}

				for( int exp =0; exp<experiments; exp++ ){

					JaccardCounterArray[][] allCounterArray = new JaccardCounterArray[ CURRENT_STRATEGIES.length ][];
					for( int i =0; i<CURRENT_STRATEGIES.length; i++ ){
						if( CURRENT_STRATEGIES[i].equals( "KBottomDensity" ) ) {				
							DensityKBottomCounterArray[] counterArray = new DensityKBottomCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								DensityKBottomCounterArray ca = new DensityKBottomCounterArray( k, 2, n );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						else if( CURRENT_STRATEGIES[i].equals( "KBottomThorup" ) ) {
							KBottomCounterArray[] counterArray = new KBottomCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								KBottomCounterArray ca = new KBottomCounterArray( k, 2, n );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						else if(	CURRENT_STRATEGIES[i].equals( "KMins" ) ) {
							KMins[] counterArray = new KMins[repeat];
							for(int j=0; j<counterArray.length; j++){
								KMins ca = new KMins( k, 2, n );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
						else if( CURRENT_STRATEGIES[i].equals( "KPartition" ) ) {
							KPartitionCounterArray[] counterArray = new KPartitionCounterArray[repeat];
							for(int j=0; j<counterArray.length; j++){
								KPartitionCounterArray ca = new KPartitionCounterArray( k, 2, n );
								counterArray[j] = ca;
							}
							allCounterArray[ i ] = counterArray;
						}
					}


					test( n, k, currentA, currentB, jaccard, allCounterArray, jaccardMean, jaccardMRAE, jaccardMRE, jaccardCVRMSE );
				}
				
				for( int cnt=0; cnt<CURRENT_STRATEGIES.length; cnt++ ) {
					String tmp= "MEAN-Summary(k=" + k + "): " + CURRENT_STRATEGIES[cnt] + " " + param + " JACCARD-MEAN " + jaccardMean[cnt];
					System.out.println( tmp );
					logInfo( tmp );
				}
				
				for( int cnt=0; cnt<CURRENT_STRATEGIES.length; cnt++ ) {
					String tmp = "MRAE-Summary(k=" + k + "): " + CURRENT_STRATEGIES[cnt] + " " + param + " JACCARD-MRAE " + jaccardMRAE[cnt];
					System.out.println( tmp );
					logInfo( tmp );
				}
				
				for( int cnt=0; cnt<CURRENT_STRATEGIES.length; cnt++ ) {
					String tmp = "MRE-Summary(k=" + k + "): " + CURRENT_STRATEGIES[cnt] + " " + param + " JACCARD-MRE " + jaccardMRE[cnt];
					System.out.println( tmp );
					logInfo( tmp );
				}
				
				for( int cnt=0; cnt<CURRENT_STRATEGIES.length; cnt++ ) {
					String tmp = "CVRMSE-Summary(k=" + k + "): " + CURRENT_STRATEGIES[cnt] + " " + param + " JACCARD-CV(RMSE) " + Math.sqrt( jaccardCVRMSE[cnt].mean() ) / jaccard ;
					System.out.println( tmp );
					logInfo( tmp );
				}
					
			}
			logInfo( "CASES IN WHICH RELATIVE ERROR is INFINITE: " + infiniteJaccardError );
			Date dt2 = new Date();
			logInfo( dt2.toString() + " Ending Experiment with parameters: " + param + " in " + (System.currentTimeMillis() - start) + "ms" );
		}



		private void test( int n, int k, IntSet cA, IntSet cB, double jaccard, JaccardCounterArray[][] counterArray, SummaryStats[] jaccardMean, SummaryStats[] jaccardMRAE, SummaryStats[] jaccardMRE, SummaryStats[] jaccardCVRMSE ) {

			boolean marked[] = new boolean[ n ];
			BooleanArrays.fill( marked, false );
			int a = 0;
			int b = 1;
			for ( int element : cA ) {
				for ( int cnt = 0; cnt < counterArray.length; cnt++ )
					for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {
						counterArray[ cnt ][ j ].add( a, element );
					}

			}
			for ( int element : cB ) {
				for ( int cnt = 0; cnt < counterArray.length; cnt++ )
					for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {
						counterArray[ cnt ][ j ].add( b, element );
					}

			}
			
			for ( int cnt = 0; cnt < counterArray.length; cnt++ ) {
				for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {
					counterArray[ cnt ][j].prepare();
				}
			}
			

			for ( int cnt = 0; cnt < counterArray.length; cnt++ ) {
				logInfo( "Estimating with " + CURRENT_STRATEGIES[ cnt ] + " with k="+k );
				SummaryStats jaccardStats = new SummaryStats();
				SummaryStats jaccardMRAEStats = new SummaryStats();
				SummaryStats jaccardMREStats = new SummaryStats();
				SummaryStats jaccardCVRMSEStats = new SummaryStats();
				for ( int j = 0; j < counterArray[ cnt ].length; j++ ) {

					if ( counterArray[ cnt ][ j ] instanceof JaccardCounterArray ) {
						double jj = ( (JaccardCounterArray)counterArray[ cnt ][ j ] ).jaccardCoefficient( a, b );
						jaccardStats.add( jj );
						jaccardMean[ cnt ].add( jj );
						logInfo( "JACCARD: " + jaccard + " JACCARD-estimate: " + jj );
						if ( jaccard == 0 && jj != 0 ) {
							infiniteJaccardError++;
							logWarn( "While using " + CURRENT_STRATEGIES[ cnt ] + ", jaccard is 0 but estimated jaccard is not" );
						}
						else {
							double jaccardMRAECurrent = jaccard == 0 ? 0 : Math.abs( jj - jaccard ) / jaccard;
							jaccardMRAEStats.add( jaccardMRAECurrent );
							jaccardMRAE[ cnt ].add( jaccardMRAECurrent );
							
							double jaccardMRECurrent = jaccard == 0 ? 0 : ( jj - jaccard ) / jaccard;
							jaccardMREStats.add( jaccardMRECurrent );
							jaccardMRE[ cnt ].add( jaccardMRECurrent );
							
							double jaccardCVRMSEcurrent = jaccard == 0 ? 0 : (jj - jaccard )*(jj - jaccard ) ;
							jaccardCVRMSEStats.add( jaccardCVRMSEcurrent );
							jaccardCVRMSE[ cnt ].add( jaccardCVRMSEcurrent );
							
						}
					}
				}
				logInfo( "STRATEGY[k="+k+"]: " + CURRENT_STRATEGIES[ cnt ] + " JACCARD: " + jaccard + " JACCARD-estimate: " + jaccardStats + " JACCARD-RelativeError: " + jaccardMRAEStats );
			}
		}
		
		
	}

	

}
