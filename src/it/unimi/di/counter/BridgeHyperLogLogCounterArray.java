package it.unimi.di.counter;

import it.unimi.dsi.util.HyperLogLogCounterArray;

public class BridgeHyperLogLogCounterArray extends HyperLogLogCounterArray implements CounterArray {

	public BridgeHyperLogLogCounterArray( int i, int n, int log2m, long nextLong ) {
		super( i, n, log2m, nextLong );
	}

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public double count2( long a ) {
		long[] aArrCount= new long[ counterLongwords ];
		getCounter( a, aArrCount );
		return count( aArrCount, 0 );
	}

	@Override
	public double union( long a, long b ) {
		long[] aArrCount= new long[ counterLongwords ];
		getCounter( a, aArrCount );
		long[] bArrCount= new long[ counterLongwords ];
		getCounter( b, bArrCount );
		max( aArrCount, bArrCount );
		return count( aArrCount, 0 );
	}

	@Override
	public double intersection( long a, long b ) {
		// TODO Auto-generated method stub
		return 0;
	}
}
