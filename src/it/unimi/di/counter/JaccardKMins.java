package it.unimi.di.counter;

import it.unimi.dsi.stat.SummaryStats;

public class JaccardKMins extends JaccardCounterArray implements CounterArray{

	final int k;
	final int m;
	final int n;
	KBottomJaccardCounterArray[] bottom1;
	
	
	public JaccardKMins ( int k, int m, int n ){
		String KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY = "RawKBottomCounterJaccardEstimateStrategy";
		this.k=k;
		this.m=m;
		this.n=n;
		bottom1 = new KBottomJaccardCounterArray[ k ];
		for( int i =0; i<k; i++ ) bottom1[ i ] = new KBottomJaccardCounterArray( 1, m, n, KBOTTOM_COUNTER_JACCARD_ESTIMATION_STRATEGY );
	}
	
	@Override
	public void add( long a, long element ) {
		for( int i =0; i<k; i++ ) bottom1[ i ].add( a, element );
	}

	@Override
	public double count( long a ) {
		SummaryStats ss = new SummaryStats();
		for( int i =0; i<k; i++ ) ss.add( bottom1[ i ].count( a ) );
		return ss.mean();
	}

	@Override
	public double union( long a, long b ) {
		SummaryStats ss = new SummaryStats();
		for( int i =0; i<k; i++ ) ss.add( bottom1[ i ].union( a, b ) );
		return ss.mean();
	}

	@Override
	public double intersection( long a, long b ) {
		SummaryStats ss = new SummaryStats();
		for( int i =0; i<k; i++ ) ss.add( bottom1[ i ].intersection( a, b ) );
		return ss.mean();
	}

	@Override
	public double jaccardCoefficient( long a, long b ) {
		SummaryStats ss = new SummaryStats();
		for( int i =0; i<k; i++ ) ss.add( bottom1[ i ].jaccardCoefficient( a, b ) );
		return ss.mean();
	}

	
	
}
