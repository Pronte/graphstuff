package it.unimi.di.counter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.util.XorShift1024StarRandom;

public class KMins extends JaccardCounterArray {

	private final static Logger LOGGER = LoggerFactory.getLogger( KMins.class );

	final int k;
	final int m;
	final int n;
	KBottomCounterArray[] bottom1;
	final long seed;

	public KMins( int k, int m, int n ) {
		this.k = k;
		this.m = m;
		this.n = n;
		seed = 131;
		bottom1 = new KBottomCounterArray[ k ];
		for ( int i = 0; i < k; i++ )
			bottom1[ i ] = new KBottomCounterArray( 1, m, n );
	}

	public KMins( int k, int m, int n, boolean collisions, long seed ) {
		this.k = k;
		this.m = m;
		this.n = n;
		this.seed = seed;
		bottom1 = new KBottomCounterArray[ k ];
		XorShift1024StarRandom random = new XorShift1024StarRandom( seed );
		for ( int i = 0; i < k; i++ )
			bottom1[ i ] = new KBottomCounterArray( 1, m, n, collisions, random.nextInt() );
	}

	@Override
	public void add( long a, long element ) {
		for ( int i = 0; i < k; i++ )
			bottom1[ i ].add( a, element );
	}

	@Override
	public double jaccardCoefficient( long a, long b ) {
		SummaryStats ss = new SummaryStats();
		for ( int i = 0; i < k; i++ )
			ss.add( bottom1[ i ].intersection( a, b ) );
		return ss.mean();
	}

	public void prepare() {
		if ( LOGGER.isTraceEnabled() ) LOGGER.trace( "Calling Prepare..." );
		for ( int i = 0; i < k; i++ )
			bottom1[ i ].sort();
	}

}
