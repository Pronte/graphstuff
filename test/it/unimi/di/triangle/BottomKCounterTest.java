package it.unimi.di.triangle;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.unimi.di.counter.KBottomCounterArray;
import it.unimi.di.counter.KBottomCounterArray.KBottomCounter;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.stat.SummaryStats;

public class BottomKCounterTest {
	
	private final static Logger LOGGER = LoggerFactory.getLogger( BottomKCounterTest.class );
	
	private static final int EXPERIMENTS = 100000;
	
	private static final int N = 1000;
	private static final int A = 30;
	private static final int B = 60;
	private static final int I = 10;
	private static final int K = 6;
	
	public double runSubsetFrequency(){

		IntArrayList bSet = new IntArrayList();
		KBottomCounterArray sketches = setup( bSet );
		int[] counter = sketches.get( 0 ).getSketch();
		int size = sketches.get( 0 ).size();
		int inters = 0;
		for( int el : bSet ) for( int i = 0; i<size; i++ ) if( el == counter[i] ) inters++;
		return inters / (double)K ;
	}
	
	public double runJaccard(){

		KBottomCounterArray sketches = setup();
		if( LOGGER.isTraceEnabled() ) LOGGER.trace( sketches.toString() );
		int sizeA = sketches.get( 0 ).size();
		int sizeB = sketches.get( 1 ).size();
		if( sizeA < K && sizeB<K ) return sketches.intersection( 0, 1 )/(double)sketches.union( 0, 1 );
		else {
			KBottomCounter union = sketches.unionCounter( 0, 1 );
			union.sort();
			KBottomCounter intersection = union.intersectionCounter( sketches.get( 0 ) );
			intersection.sort();
			if( LOGGER.isTraceEnabled() ) LOGGER.trace( "Union counter for the first k: " + union.size() + " elements:" + union.toString() + " when A is " + sketches.get( 0 ).toString() + " and B is " + sketches.get( 1 ) );
			if( LOGGER.isTraceEnabled() ) LOGGER.trace( "Intersection1: " + intersection + " Intersection2:" + intersection.intersectionCounter( sketches.get( 1 ) ).toString() );
			return intersection.intersection( sketches.get( 1 ) )/(double)K;
		}
	
	}
	
	public KBottomCounterArray setup( IntArrayList bSet ){
		KBottomCounterArray sketches = new KBottomCounterArray( K, 1, N );
		int nodes = 0;
		for( int i=0; i<A-I; i++ ){
			sketches.add( 0, nodes );
			nodes ++;
		}
		for( int i=0; i<B-I; i++ ){
			bSet.add( nodes );
			nodes ++;
		}
		for( int i=0; i<I; i++ ){
			sketches.add( 0, nodes );
			bSet.add( nodes );
			nodes++;
		}
		sketches.sort();
		return sketches;
	}

	public static KBottomCounterArray setup(){
		KBottomCounterArray sketches = new KBottomCounterArray( K, 2, N );
		int nodes = 0;
		for( int i=0; i<A-I; i++ ){
			sketches.add( 0, nodes );
			nodes ++;
		}
		for( int i=0; i<B-I; i++ ){
			sketches.add( 1, nodes );
			nodes ++;
		}
		for( int i=0; i<I; i++ ){
			sketches.add( 0, nodes );
			sketches.add( 1, nodes );
			nodes++;
		}
		sketches.sort();
		return sketches;
	}
	
	@Test
	public void testSubsetFrequency(){
		SummaryStats ss = new SummaryStats();
		double exact = I/(double)A;
		for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
			ss.add( runSubsetFrequency() );
			System.out.println( "Frequency Estimated " + ss.mean() + " and exact " + exact );
		}
		Assert.assertEquals( "Frequency Estimated " + ss.mean() + " and exact " + exact,  exact, ss.mean(), exact/100. );	
	}
	
	@Test
	public void testJaccard(){
		SummaryStats ss = new SummaryStats();
		double exact = I/(double)(A+B-I);
		for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
			ss.add( runJaccard() );
			System.out.println( "Frequency Estimated " + ss.mean() + " and exact " + exact );
		}
		Assert.assertEquals( "Frequency Estimated " + ss.mean() + " and exact " + exact,  exact, ss.mean(), exact/100. );	
	}
	
}
