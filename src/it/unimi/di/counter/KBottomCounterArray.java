package it.unimi.di.counter;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.jet.random.engine.MersenneTwister;
import it.unimi.dsi.Util;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntArrays;
import it.unimi.dsi.fastutil.ints.IntSemiIndirectHeaps;
import it.unimi.dsi.util.XorShift1024StarRandom;


public class KBottomCounterArray extends JaccardCounterArray implements CounterArray {

	private final static Logger LOGGER = LoggerFactory.getLogger( KBottomCounterArray.class );
	
	protected final int[] element2Rank;
	protected KBottomCounter[] counters;
	protected final int k;

	private boolean sorted;
	
	public KBottomCounterArray( int k, int m, int n ){
		XorShift1024StarRandom random = new XorShift1024StarRandom();
		element2Rank = IntArrays.shuffle( Util.identity( n ), random );
		for( int i = n; i-- != 0; ) element2Rank[ i ] = -element2Rank[ i ];
		this.k = k;
		counters = new KBottomCounter[ m ];
		for ( int i = 0; i < counters.length; i++ ) {
			counters[ i ] = new KBottomCounter();
		}
	}
	
	public KBottomCounterArray( int k, int m, int n, boolean allowCollision, int seed ){
		if( !allowCollision ){
			XorShift1024StarRandom random = new XorShift1024StarRandom();
			element2Rank = IntArrays.shuffle( Util.identity( n ), random );
			for( int i = n; i-- != 0; ) element2Rank[ i ] = -element2Rank[ i ];
			this.k = k;
			counters = new KBottomCounter[ m ];
			for ( int i = 0; i < counters.length; i++ ) {
				counters[ i ] = new KBottomCounter();
			}
		}
		else{
			LOGGER.warn( "Allowing collisions..." );
			//XorShift1024StarRandom random = new XorShift1024StarRandom();
			MersenneTwister random = new MersenneTwister( seed );
			element2Rank = new int[ n ];
			for( int i = n; i-- != 0; ) element2Rank[ i ] = -random.nextInt();
			this.k = k;
			counters = new KBottomCounter[ m ];
			for ( int i = 0; i < counters.length; i++ ) {
				counters[ i ] = new KBottomCounter();
			}			
		}
	}

	public KBottomCounter get( int i ){
		return counters[ i ];
	}
	
	@Override
	public void add( long a, long element ) {
		if( sorted ) throw new IllegalStateException();
		counters[ (int)a ].add( (int)element );
	}

	@Override
	public double count( long a ) {
		throw new RuntimeException( "The method count has not been implemented" );
	}


	public KBottomCounter unionCounter( long a, long b ) {
		if( !sorted ) throw new IllegalStateException();
		return counters[(int)a].unionCounter( counters[(int)b] );
	}
	
	@Override
	public double union( long a, long b ) {
		if( !sorted ) throw new IllegalStateException();
		return counters[(int)a].union( counters[(int)b] );
	}
	
	public KBottomCounter intersectionCounter( long a, long b ) {
		if( !sorted ) throw new IllegalStateException();
		return counters[(int)a].intersectionCounter( counters[(int)b] );
	}
	
	public String toString(){
		String rst = "";
		for( KBottomCounter sk : counters ) rst+=sk.toStringWithRanking()+"\n";
		return rst;
	}
	
	
	@Override
	public double intersection( long a, long b ) {
		if( !sorted ) throw new IllegalStateException();
		return counters[(int)a].intersection( counters[(int)b] );
	}
	
	public void sort() {
		if( sorted ) return;
		sorted = true;
		for( KBottomCounter c : counters ) {
			if( LOGGER.isTraceEnabled() ) LOGGER.trace( "Before sorting " + c.toStringWithRanking() ); 
			c.sort();
		}
	}
	
	public class KBottomCounter{
		private final int[] sketch;
		private int size;
		private int maxRank;
		private int elementMaxRank;
		
		public KBottomCounter(){
			sketch = new int[ k ];
		}

		public KBottomCounter( IntArrayList elements ){
			sketch = new int[ k ];
			for( int a : elements ) add( a );//We are not assuming elements is not ordered even if it is 
		}
		

		public int[] getSketch(){
			return sketch;
		}
		

		public void add( int element ){
			if ( size < k ) {
				sketch[ size++ ] = element;
				IntSemiIndirectHeaps.upHeap( element2Rank, sketch, size, size - 1, null );
			}
			else if ( element2Rank[ element ] > element2Rank[ sketch[ 0 ] ] ) {
				sketch[ 0 ] = element;
				IntSemiIndirectHeaps.downHeap( element2Rank, sketch, size, 0, null );
			}
		}

		public void sort() {
			maxRank = - element2Rank[ sketch[0] ];
			elementMaxRank = sketch[0];
			IntArrays.quickSort( sketch, 0, size );
		}
		
		
		public KBottomCounter intersectionCounter( KBottomCounter b ){
			IntArrayList l = new IntArrayList();
			for( int i = 0, j = 0; i < size && j < b.size; ) {
				final int c = Integer.compare( sketch[ i ], b.sketch[ j ] );
				if ( c == 0 ) {
					l.add( sketch[ i ] );
					i++;
					j++;
				}
				else if ( c == -1 ) i++;
				else j++;
			}
			return new KBottomCounter( l );
		}
		
		public KBottomCounter unionCounter( KBottomCounter b ){
			IntArrayList l = new IntArrayList();
			for( int i = 0, j = 0; i < size || j < b.size; ) {
				final int c = i >= size ? 1: j>=b.size ? -1 : Integer.compare( sketch[ i ], b.sketch[ j ] );
				if ( c > 0 ) {
					int temp = b.sketch[ j++ ];
					l.add( temp );
				}
				else if ( c < 0 ) l.add( sketch[ i++ ] );
				else {
					l.add( b.sketch[ j++ ] );
					i++;
				}
			}
			return new KBottomCounter( l );
		}
		
		public double unionLessThanAThreshold( KBottomCounter b, int th ){
			IntArrayList l = new IntArrayList();
			for( int i = 0, j = 0; i < size || j < b.size; ) {
				final int c = i >= size ? 1: j>=b.size ? -1 : Integer.compare( sketch[ i ], b.sketch[ j ] );
				if ( c > 0 ) {
					if( -element2Rank[ b.sketch[ j ] ] <=th ) l.add( b.sketch[ j ] ); 
					j++;
				}
				else if ( c < 0 ){
					if( -element2Rank[sketch[ i ] ] <=th ) l.add( sketch[ i ] );
					i++;
				}
				else {
					if( -element2Rank[ b.sketch[ j ] ] <=th ) l.add( b.sketch[ j ] );
					j++;
					i++;
				}
			}
			return l.size();
		}
		
		public double union( KBottomCounter b ){
			int rst = 0;
			for( int i = 0, j = 0; i < size || j < b.size; ) {
				final int c = i >= size ? 1: j>=size ? -1 : Integer.compare( sketch[ i ], b.sketch[ j ] );
				if ( c > 0 ) {
					rst++;
					j++;
				}
				else if ( c < 0 ) {
					rst++;
					i++;
				}
				else {
					j++;
					i++;
					rst++;
				}
			}
			return rst;
		}
		
		public String toString(){
			return Arrays.toString( Arrays.copyOfRange( sketch, 0, size ) );
		}
		
		public String toStringWithRanking(){
			String rst="[";
			for( int i =0; i<size; i++) rst+=" "+sketch[i]+":"+element2Rank[sketch[i]];
			return rst + "]";
		}
		
		public double intersection( KBottomCounter b ){
			int rst = 0;
			//LOGGER.debug( "Doing intersection between " + this + " and " + b );
			for( int i = 0, j = 0; i < size && j < b.size; ) {
				final int c = Integer.compare( sketch[ i ], b.sketch[ j ] );
				if ( c == 0 ) {
					//LOGGER.debug( "Intersection " + sketch[ i ] );
					rst++;
					i++;
					j++;
				}
				else if ( c == -1 ) i++;
				else j++;
			}
			return rst;
		}
		
		
		public int size(){
			return size;
		}
		
		public int getMaximumRank(){
			if( !sorted ) throw new IllegalStateException();
			return maxRank;
		}
		
		public int getElementMaximumRank(){
			if( !sorted ) throw new IllegalStateException();
			return elementMaxRank;
		}

		public double jaccardCoefficient( KBottomCounter kBottomCounter ) {
			if( !sorted ) throw new IllegalStateException();
			
			if( LOGGER.isTraceEnabled() ) LOGGER.trace( "Calling Jaccard coeff with input " + toStringWithRanking() + " and " + kBottomCounter.toStringWithRanking() );
			
			int max1 = size() <k ? Integer.MAX_VALUE : getMaximumRank();
			int max2 = size() <k ? Integer.MAX_VALUE : kBottomCounter.getMaximumRank();
			int th = Math.min( max1, max2 );
			if( LOGGER.isTraceEnabled() ) LOGGER.trace( "The maximum ranks are " + max1 + " and " + max2 + " while threshold is " + th );
			
			double inters = intersection( kBottomCounter );
			double union = unionLessThanAThreshold( kBottomCounter, th );
			if( LOGGER.isTraceEnabled() )LOGGER.trace( "The intersection is " + inters + " while the union is " + union );
			return inters/(double)union;
		}
	}

	@Override
	public double jaccardCoefficient( long v, long w ){
		KBottomCounter union = unionCounter( v, w );
		union.sort();
		KBottomCounter intersection = union.intersectionCounter( get( (int)v ) );
		intersection.sort();
		double j = intersection.intersection( get( (int)w ) )/(double)k;
		return j;
	}
	
	@Override
	public void prepare(){
		if( LOGGER.isTraceEnabled() ) LOGGER.trace( "Calling Prepare..." );
		sort();
	}

}

