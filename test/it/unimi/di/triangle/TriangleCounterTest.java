package it.unimi.di.triangle;

import it.unimi.di.triangle.AsymmetricApproximateTriangleCounterBottomK;
import it.unimi.di.triangle.ExactTriangleCounter;
import it.unimi.dsi.stat.SummaryStats;
import it.unimi.dsi.webgraph.ArrayListMutableGraph;
import it.unimi.dsi.webgraph.ImmutableGraph;
import it.unimi.dsi.webgraph.Transform;
import it.unipi.di.generators.CliqueGraph;
import it.unipi.di.generators.ErdosRenyiGraph;
import it.unipi.di.generators.GraphGenerator;
import it.unipi.di.generators.StarGraph;

import org.junit.Assert;
import org.junit.Test;

public class TriangleCounterTest {
	
	private final static int EXPERIMENTS = 2000;
	private final static int K = 5;
	private static int NODE_TO_BE_TEST = 0;
	private static int EDGE_TO_BE_TEST = 0;
	
	
	@Test
	public void testExact2(){
		ImmutableGraph graph = new ArrayListMutableGraph( 5, new int[][] {
				{0,1},
				{0,2},
				{1,2},
				{1,3},
				{2,3},
				{3,4}
		} ).immutableView();
		ImmutableGraph symmetricGraph = Transform.symmetrize( graph );
		Assert.assertEquals( new ExactTriangleCounter( symmetricGraph ).getNumberOfTriangles(), 2, 0 );
	}
	
	@Test
	public void testExact1(){
		ImmutableGraph graph = new ArrayListMutableGraph( 3, new int[][] {
				{0,1},
				{1,2},
				{2,0}
		} ).immutableView();
		ImmutableGraph symmetricGraph = Transform.symmetrize( graph );
		Assert.assertEquals( new ExactTriangleCounter( symmetricGraph ).getNumberOfTriangles(), 1, 0 );
	}
	
	@Test
	public void testBottomK(){
		ImmutableGraph graph = new ArrayListMutableGraph( 3, new int[][] {
				{0,1},
				{1,2},
				{2,0}
		} ).immutableView();
		ImmutableGraph symmetricGraph = Transform.symmetrize( graph );
		Assert.assertEquals( new AsymmetricApproximateTriangleCounterBottomK(symmetricGraph, 4 ).getNumberOfTriangles(), 1, 0 );
	}
	
	
	private void testNode( GraphGenerator ttt ){
		ImmutableGraph graph = new ArrayListMutableGraph( ttt.getNumberOfNodes(), ttt.getGraph() ).immutableView();		
		ImmutableGraph graph2 = Transform.symmetrize( graph );
		SummaryStats ss = new SummaryStats();
		if ( NODE_TO_BE_TEST != -1 ) {
			double exact = new ExactTriangleCounter( graph2 ).getNumberOfTrianglesForEachNode()[ NODE_TO_BE_TEST ];
			for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
				ss.add( new AsymmetricApproximateTriangleCounterBottomK(graph2, K ).getNumberOfTrianglesForEachNode()[ NODE_TO_BE_TEST ] );
				System.out.println( "Analyzing node " + NODE_TO_BE_TEST + " having triangles estimated " + ss.mean() + " and exact " + exact );
				System.out.println( "Stats " + ss.toString() );
			}
			Assert.assertEquals( "Analyzing node " + NODE_TO_BE_TEST + " " ,  exact , ss.mean(), exact/10. );
		}
	}
	
	private void testEdge( GraphGenerator ttt ){
		ImmutableGraph graph = new ArrayListMutableGraph( ttt.getNumberOfNodes(), ttt.getGraph() ).immutableView();		
		ImmutableGraph graph2 = Transform.symmetrize( graph );
		SummaryStats ss = new SummaryStats();
		if ( EDGE_TO_BE_TEST != -1 ) {
			double exact = new ExactTriangleCounter( graph2 ).getNumberOfTrianglesForEachEdge().getDouble( EDGE_TO_BE_TEST );
			for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
				ss.add( new AsymmetricApproximateTriangleCounterBottomK(graph2, K ).getNumberOfTrianglesForEachEdge().getDouble( EDGE_TO_BE_TEST ) );
				System.out.println( "Analyzing edge " + EDGE_TO_BE_TEST + " having triangles estimated " + ss.mean() + " and exact " + exact );
				System.out.println( "Stats " + ss.toString() );
			}
			Assert.assertEquals( "Analyzing edge " + EDGE_TO_BE_TEST ,  exact , ss.mean(), exact/10. );
		}
	}
	
	private void testTriangle( GraphGenerator ttt ){
		ImmutableGraph graph = new ArrayListMutableGraph( ttt.getNumberOfNodes(), ttt.getGraph() ).immutableView();		
		ImmutableGraph graph1 = Transform.symmetrize( graph );
		ImmutableGraph graph2 = Transform.filterArcs( graph1, Transform.NO_LOOPS );
		SummaryStats ss = new SummaryStats();
		double exact = new ExactTriangleCounter( graph2 ).getNumberOfTriangles();
		for ( int exp = 0; exp <EXPERIMENTS; exp++ ){
			ss.add( new AsymmetricApproximateTriangleCounterBottomK(graph2, K ).getNumberOfTriangles() );
			System.out.println( "Triangles estimated " + ss.mean() + " and exact " + exact );
			System.out.println( "Stats " + ss.toString() );
		}
		Assert.assertEquals( "",  exact, ss.mean(), exact/10. );		
	}
	
	//private final static int[][] DEGREEs = { {32, 32}, {32, 256}, {32, 1024}, {256, 256}, {256, 1024}, {1024, 1024} };
	private final static int[][] DEGREEs = { {32, 32} };
	//private final static int STEP_COMMON_NEIGHs = 8;
		
	@Test
	public void testStarNode(){
		for( int[] p : DEGREEs ){
			int i = p[0];
			int j = p[1];
			//for( int tr=0; tr<Math.min( i, j ); tr+=Math.min( i, j )/STEP_COMMON_NEIGHs ){
			int tr=4;
			
			StarGraph ttt= new StarGraph( i, j, tr );
			testNode( ttt );
		}
	}
	
	@Test
	public void testStarEdge(){
		for( int[] p : DEGREEs ){
			int i = p[0];
			int j = p[1];
			//for( int tr=0; tr<Math.min( i, j ); tr+=Math.min( i, j )/STEP_COMMON_NEIGHs ){
			int tr=4;
			
			StarGraph ttt= new StarGraph( i, j, tr );
			testEdge( ttt );
		}
	}
	
	@Test
	public void testStarTriangles(){
		for( int[] p : DEGREEs ){
			int i = p[0];
			int j = p[1];
			//for( int tr=0; tr<Math.min( i, j ); tr+=Math.min( i, j )/STEP_COMMON_NEIGHs ){
			int tr=4;

			StarGraph ttt= new StarGraph( i, j, tr );
			testTriangle( ttt );
		}
	}
	
	
	private static final int N = 100;
	private static final int M = 20;
	
	@Test
	public void testErdosRenyiiTriangle(){
		ErdosRenyiGraph ttt= new ErdosRenyiGraph( N, M, 131 );
		testTriangle( ttt );
	}
	
	@Test
	public void testErdosRenyiiNode(){
		ErdosRenyiGraph ttt= new ErdosRenyiGraph( N, M, 131 );
		testNode( ttt );
	}
	
	@Test
	public void testErdosRenyiiEdge(){
		ErdosRenyiGraph ttt= new ErdosRenyiGraph( N, M, 131 );
		testEdge( ttt );
	}
	
	
	@Test
	public void testCliqueTriangle(){
		CliqueGraph ttt= new CliqueGraph( N );
		testTriangle( ttt );
	}
	
	@Test
	public void testCliqueNode(){
		CliqueGraph ttt= new CliqueGraph( N );
		testNode( ttt );
	}
	
	@Test
	public void testCliqueEdge(){
		CliqueGraph ttt= new CliqueGraph( N );
		testEdge( ttt );
	}
	
	
	
	
	public static void main( String[] args ){
		//new TriangleCounterTest().testCliqueNode();
		new TriangleCounterTest().testCliqueNode();
	}
	
	
}
