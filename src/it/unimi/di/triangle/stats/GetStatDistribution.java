package it.unimi.di.triangle.stats;
import it.unimi.di.triangle.test.ExactVsHyperLogLog;
import it.unimi.dsi.stat.SummaryStats;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.UnflaggedOption;


public class GetStatDistribution {

	public static void main( String[] arg ) throws JSAPException, IOException {

		SimpleJSAP jsap = new SimpleJSAP( ExactVsHyperLogLog.class.getName(),
				"Given a list of files (two columns id value) separated by , compute id and means of values. "
						+ "If a target file is set the MRE is computed for each id for ",
				new Parameter[] {
						new FlaggedOption( "target", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 't', "target", "the target file" ),
						new UnflaggedOption( "files", JSAP.STRING_PARSER, JSAP.REQUIRED, "The list of files to be processed." )

				} );


		JSAPResult jsapResult = jsap.parse( arg );
		if ( jsap.messagePrinted() ) System.exit( 1 );

		String[] list = jsapResult.getString( "files" ).trim().split( "," );
		List<String> a1 = Files.readLines( new File( list[ 0 ] ), Charsets.UTF_8 );
		
		int size=-1;
		if ( jsapResult.userSpecified( "target" ) ) size=Files.readLines( new File( jsapResult.getString( "target" ) ), Charsets.UTF_8 ).size();
		else size = a1.size();
		
		SummaryStats[] ss = new SummaryStats[ size ];
		//SummaryStats[] ssMRE = new SummaryStats[ size ];
		//SummaryStats[] ssMRAE = new SummaryStats[ size ];
		SummaryStats[] ssCVRMSE = new SummaryStats[ size ];
		for ( int y = 0; y < ss.length; y++ ) {
			ss[ y ] = new SummaryStats();
			//ssMRE[ y ] = new SummaryStats();
			//ssMRAE[ y ] = new SummaryStats();
			ssCVRMSE[ y ] = new SummaryStats();
		}

		boolean comparison = false;
		double[] targetValues = new double[ size ];
		double maxTarget = -1;
		int maxid = -1;
		if ( jsapResult.userSpecified( "target" ) ) {
			comparison = true;
			List<String> values = Files.readLines( new File( jsapResult.getString( "target" ) ), Charsets.UTF_8 );
			for ( String line : values ) {
				int id = Integer.parseInt( line.split( " " )[ 0 ] );
				double value = Double.parseDouble( line.split( " " )[ 1 ] );
				targetValues[ id ] = value;
				if ( value > maxTarget ) {
					maxTarget = value;
					maxid = id;
				}
			}
		}
		System.out.println( "Maximum target is " + maxTarget + "because of node " + maxid );
		//SummaryStats[] targetDistMRE = new SummaryStats[ (int)( maxTarget + 1 ) ];
		//SummaryStats[] targetDistMRAE = new SummaryStats[ (int)( maxTarget + 1 ) ];
		SummaryStats[] targetDistCVRMSE = new SummaryStats[ (int)( maxTarget + 1 ) ];
		for ( int i = 0; i < targetDistCVRMSE.length; i++ ) {
			targetDistCVRMSE[ i ] = new SummaryStats();
			//targetDistMRAE[ i ] = new SummaryStats();
			//targetDistMRE[ i ] = new SummaryStats();
		}


		for ( String ff : list ) {
			List<String> a = Files.readLines( new File( ff ), Charsets.UTF_8 );
			if ( a.size() > ss.length ) throw new RuntimeException( "The files must contain at most the same number of lines of the target file or the first file" );
			if ( a.size() != ss.length ) System.err.println( "The files should contain the same number of lines of the target file or the first file" );
			for ( String line : a ) {
				int id = Integer.parseInt( line.split( " " )[ 0 ] );
				double value = Double.parseDouble( line.split( " " )[ 1 ] );
				ss[ id ].add( value );
				if ( comparison && targetValues[ id ] != 0 ) {
					//double mre = ( value - targetValues[ id ] ) / targetValues[ id ];
					//ssMRE[ id ].add( mre );
					//double mrae = Math.abs( value - targetValues[ id ] ) / targetValues[ id ];
					//ssMRAE[ id ].add( mrae );
					double cvrmse = ( value - targetValues[ id ] ) * ( value - targetValues[ id ] );
					ssCVRMSE[ id ].add( cvrmse );
				}
			}
		}

		System.out.println( "distribution " );
		if ( comparison ) {
			//SummaryStats summaryMRE = new SummaryStats();
			//SummaryStats summaryMRAE = new SummaryStats();
			SummaryStats summaryCVRMSE = new SummaryStats();
			for ( int y = 0; y < ss.length; y++ ) {
				System.out.println( "MEAN " + y + " " + ss[ y ].mean() );
				if ( targetValues[ y ] != 0 ) {
					//System.out.println( "MRE " + y + " " + ssMRE[ y ].mean() );
					//System.out.println( "MRAE " + y + " " + ssMRAE[ y ].mean() );
					double finalCVRMSE = Math.sqrt( ssCVRMSE[ y ].mean() ) / targetValues[ y ];
					System.out.println( "CVRMSE " + y + " " + finalCVRMSE );
					//summaryMRE.add( ssMRE[ y ].mean() );
					//summaryMRAE.add( ssMRAE[ y ].mean() );
					summaryCVRMSE.add( finalCVRMSE );
					
					//targetDistMRE[ (int)targetValues[ y ] ].add( ssMRE[ y ].mean() );
					//targetDistMRAE[ (int)targetValues[ y ] ].add( ssMRAE[ y ].mean() );
					targetDistCVRMSE[ (int)targetValues[ y ] ].add( finalCVRMSE );

				}
			}
			//System.out.println( "Summary-MRE " + summaryMRE.mean() );
			//System.out.println( "Summary-MRAE " + summaryMRAE.mean() );
			System.out.println( "Summary-CVRMSE " + summaryCVRMSE.mean() );
			for ( int y = 1; y < targetDistCVRMSE.length; y++ ) {
				if(targetDistCVRMSE[ y ].size64()==0) continue;
				double finalCvrmse = Math.sqrt( targetDistCVRMSE[ y ].mean() ) / y;
				//double mre = targetDistMRE[ y ].mean();
				//double mrae = targetDistMRAE[ y ].mean();
				System.out.println( "CVRMSE DISTRIBUTION " + y + " " + finalCvrmse + " - nodes with "+y+" triangles " + targetDistCVRMSE[ y ].size64() );
			}

		}
		else {
			for ( int y = 0; y < ss.length; y++ ) {
				System.out.println( y + " " + ss[ y ].mean() );
			}
		}

	}

}
