package it.unipi.di.generators;


public abstract class GraphGenerator {

	protected int nodes;
	protected int[][] result;
	
	public int getNumberOfNodes(){
		return nodes;
	}

	public int[][] getGraph(){
		return result;
	}
	
	public String toString(){
		String rst = ""+nodes +"\n";
		for( int i=0; i<result.length; i++ ){
			rst+=",("+result[i][0]+","+result[i][1]+")";
		}
		return rst;
	}
	
}
