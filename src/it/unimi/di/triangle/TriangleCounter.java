package it.unimi.di.triangle;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;



public interface TriangleCounter {

	public double getNumberOfTriangles();
	
	public double[] getNumberOfTrianglesForEachNode();
	
	public DoubleArrayList getNumberOfTrianglesForEachEdge();
	
}
